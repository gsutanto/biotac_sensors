#include <ros/ros.h>

#include <biotac_sensors/BioTacHand.h>
#include <biotac_sensors/biotacSrv.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>

#include <stdio.h>
#include <iostream>
#include <fstream>

#define electroid_length 19
#define data_channels 23
#define pac_length 22
#define max_components 8

#define hunter_biotac_feature_extraction
#define file_extension ""
class BiotacFeatureExtraction
{
  ros::NodeHandle n_;
  ros::Subscriber bioTac_sub_;
  ros::ServiceServer biotac_srv_;
public:
  cv::Mat raw_data;
  cv::Mat subtracted_mean;
  cv::Mat eigenmatrix;
  bool collect_data;
  bool start_up;
  bool training_mode;
  int count;
  BiotacFeatureExtraction(ros::NodeHandle n):n_(n),collect_data(false),start_up(true),subtracted_mean(1,data_channels,CV_64FC1,0.0)
  {
    ros::NodeHandle n_private("~");
    n_private.param("training_mode",training_mode,true);
    //n_private.param("file_extension",file_extension,"");
    bioTac_sub_ = n_.subscribe("biotac_pub",1,&BiotacFeatureExtraction::bioCallback,this);
    biotac_srv_ = n_.advertiseService("biotac_srv",&BiotacFeatureExtraction::biotacSrv,this);
    ROS_INFO("Biotac Feature Extraction Initialized");
    if(!training_mode)
    {
      // if not training then need to read in eigenmatrix
      readMat(eigenmatrix,"biotac_eigenvector_matrix.txt",data_channels);
    }
  }

  bool biotacSrv(biotac_sensors::biotacSrv::Request & req,
		 biotac_sensors::biotacSrv::Response & res)
  {
    /* turns on and off the biotac sensor, when shuts off returns reading */

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!NOT TESTED PLEASE CONFIRM BEFORE TRUSTING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! need to add support for a pre-defined eigenmatrix
    if(req.state)
    {
      // turn on
      collect_data = true;
      res.newState = true;
    }
    else
    {
      // turn off and return reading
      res.newState = false;
      collect_data = false;
      if(training_mode)
      {
	writeMat(raw_data,"biotac_training_data.txt");
      }
      else
      {
      }
    }
    if(req.completed_training)
    {
      completeTraining();
    }
    return(1);
  }

  void completeTraining()
  {
    /* creates the PCA simplification matrix and saves to a file */
    cv::Mat data;
    readMat(data,"testfile.txt",3);//data_channels);
    std::cout << data << "\n";
    cv::PCA pca_analysis(data,cv::Mat(),CV_PCA_DATA_AS_ROW,2);//,max_components);
    cv::Mat eigenMat = pca_analysis.eigenvectors;
    writeMat(eigenMat,"biotac_eigenvector_matrix.txt");
  }

  void bioCallback(biotac_sensors::BioTacHand bt_hand)
  {
    /* collects large mat with data from biotac */
    if(collect_data || start_up)
    {
      // place incoming data into holder mat
      biotac_sensors::BioTacData bt_data = bt_hand.bt_data[0];
      double mean = 0;
      //std::cout << "pac_data length=" << bt_data.pac_data.size() << "\n";
      for(int i=0;i<pac_length;i++)
      {
	mean += static_cast<double>(bt_data.pac_data[i]);
      }
      mean /= pac_length;
      cv::Mat holder(1,data_channels,CV_64FC1);
      holder.at<double>(0,0) = static_cast<double>(bt_data.tdc_data);
      holder.at<double>(0,1) = static_cast<double>(bt_data.tac_data);
      holder.at<double>(0,2) = static_cast<double>(bt_data.tdc_data);
      holder.at<double>(0,3) = mean;
      for(int i=0;i<electroid_length;i++)
      {
	holder.at<double>(0,i+4) = bt_data.electrode_data[i];
      }
    
      if(start_up)
      {
	subtracted_mean = subtracted_mean + holder;
	count++;
	if(count >=1000)
	{
	  subtracted_mean /= 1000;
	  start_up = false;
	}
      }
      if(collect_data)
      {
	holder -= subtracted_mean;
	raw_data.push_back(holder);
      }
    }
  }

  void writeMat(cv::Mat data, const char* filename)
  {
    /* write the data to a file */
    std::ofstream fout;
    fout.open(filename,std::ios::app);
    for(int i=0;i<data.rows;i++)
    {
      for(int j=0;j<data.cols;j++)
      {
	fout << data.at<double>(i,j) << "\t";
      }
      fout << "\n";
    }
    fout.close();
  }

  void readMat(cv::Mat& data,const std::string& filename, int data_length)
  {
    /* reads in the mat data */
    std::ifstream is(filename.c_str());
    cv::Mat holder(1,data_length,CV_64FC1);
    for(std::string line;std::getline(is,line);)
    {
      std::istringstream in(line);
      for(int i=0;i<data_length;i++)
      {
	double x;
	in >> x;
	holder.at<double>(0,i) = x;
      }
      data.push_back(holder);
    }
  }

  void spin()
  {
    while(n_.ok())
    {
      ros::spinOnce();
    }
  }
  
};


int main(int arg, char **argv)
{
  ros::init(arg,argv,"BioTac_Feature_Extractor");
  ros::NodeHandle nh;
  BiotacFeatureExtraction BFE(nh);
  BFE.completeTraining();
  BFE.spin();
}
