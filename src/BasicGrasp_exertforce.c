#include "BasicGrasp_task.h"
#include "netRobot.h"
#include "math.h"
#include "biotac_sl.h"
#include "SL_user.h"
#include "rudisLittleQuatShow.h"
#include "online_classification.h"
#include<pthread.h>

//#include "rdf.h"

// #define DEBUG_FORCES 1
//#define DEBUG_FORCES_START 1
// #define DEBUG_PARAMETER_INIT 1
// #define DEBUG_FORCE_TRANSFORMS 1
#define J_INV_CONTROL 1
#define DEBUG_HYBRID_FORCE_CONTROL 1
// #define DEBUG_CONTACT_THRESH_CONTROL 1
#define DEBUG_CONTACT_ESTIMATION 1
#define BIOTAC_ATTACHED 1

extern int GotoState;
extern SL_Cstate  ctarget[N_ENDEFFS+1];
extern SL_quat    ctarget_quat[N_ENDEFFS+1];

static int use_at_desired_force = FALSE;
enum ForceControlType current_controller = FORCE_FEEDBACK_CONTROL;

//
char* cur_log_name = "";
size_t cur_log_name_length = 0;
char* log_key_file_name = "log_file_key.txt";

// Timing variables
static int timing_force_move = 0;
static double des_force_move_time = 0.0;
static double force_move_start_time;
static double slip_start_time;
static int init_pos_reached = 0;

// Force control variables
static Vector f_world_k_minus_1;
static Vector f_desired;
static double sigma_P_f;
static double sigma_P_t;
static double sigma_D_f;
static double sigma_D_t;
static Matrix Sigma_P;
static Matrix Sigma_D;
static Vector f_diff_P;
static Vector f_diff_D;
static Matrix neg_p_ee_hat;
static Vector f_s;
static Matrix transform_ll;
static Matrix force_transform_mat;
static SL_quat delta_Q_target;
static Vector delta_O_target;

// Hybrid force-velocity control variables
static Vector force_sensed_w3;
static double force_integral;
static double MAX_FORCE_INTEGRAL_MAGNITUDE;
static double force_gain_I;
static double pressure_delta;
static double pressure_Integral;
static double force_gain_P;
static double pressure_gain_P;
static double pressure_gain_D;
static double pressure_gain_I;
static double force_normal_desired;
static double force_contact_thresh;
static Vector u_force;
static Vector u_velocity;
static Vector u_hybrid;
static Vector v_d_k; // Desired feed forward velocity
static double slip_dc_init_pos[7] = {0,0,0,0,0,0,0};
static  Matrix P_bar;
static int desired_force_count_;
static int controller_running_counter_;
static int joint_position_control = 0;

// Biotac Values
static double PDC_contact_threshold;
static Vector c_pos_tare;
static Vector c_normal_tare;
static int c_pos_tare_init;
static int in_contact;
static int stability_counter;
static int inital_stability;
static int stability_filter;
static int has_moved_back;
static int reset_random_vel;
Matrix elect_position;
Matrix elect_normal;
char* object_name = "";
char* exp_type = "";
char* training_type = "";
char* classifier_type = "";
char* train_names = "";
char* train_trials = "";
char* target_name = "";
char* target_trials = "";
char* feature_type = "";
char* prediction_type = "";
static Vector FT_tare;
double ext_x;
double ext_y;
double PDC_good;

struct rdf_arg args;
int class_mode_select = 0;
float c_response = 0.0;


#ifdef __cplusplus
extern "C" {
#endif
extern int Class_value;
extern int Class_Trained;
extern int classifier_active;
int thread_launched = 0;
#ifdef __cplusplus
}
#endif

// Constants
static double DESIRED_FORCE_EPSILON;
static int DESIRED_FORCE_COUNT_THRESH;
static double ENDEFFECTOR_MASS;

// Contact state estimation variables
static Matrix Gamma_r;
static float gamma_r;
static float beta_r;
static float gamma_n;
static float beta_n;
static Matrix L_r_t;
static Vector r_hat_e_t;
static Vector c_r_t;
static Matrix L_n_t;
static Vector n_hat_c_t;
static Vector p_hat_c_t;

// Transform functions
static Matrix I3, a_mat, a_prod;
static Matrix R_AB_trans, P_AB_hat_neg, W_transform_AB;
static Matrix R_sw, R_ws; // Transpose (inverse) of R_sw
static Vector P_sw, P_ws;

void initialize_exert_force()
{
	static int i, r, c;
	// TODO: put these on the paramter server thingy
	// NOTE: Turning off torque gains as we dont have the correct angle calibration for the
	// kinematic transform in the sensor frame
	sigma_P_f = 5e-2; // P term for force
	sigma_P_t = 0.0;// 1e-5; // P term for torque
	sigma_D_f = 5e-3;  // D term for force
	sigma_D_t = 0.0; //5e-6;  // D term for torque

	elect_position = my_matrix(1,19,1,3);
	elect_normal = my_matrix(1,19,1,3);

	PDC_tare = misc_sensor[bt_PDC];

	for (i = 0; i < 19; ++i)
	{
	       electrode_tare[i] += misc_sensor[bt_E01+i];
	}

	FT_tare = my_vector(1,6);

	for(i = 1; i<=6; i++)
	{
		FT_tare[i] = misc_sensor[i];
	}

	elect_position[1][1] = 0.993;
	elect_position[1][2] = -4.855;
	elect_position[1][3] = -1.116;
	elect_position[2][1] = -2.700;
	elect_position[2][2] = -3.513;
	elect_position[2][3] = -3.670;
	elect_position[3][1] = -6.200;
	elect_position[3][2] = -3.513;
	elect_position[3][3] = -3.670;
	elect_position[4][1] = -8.000;
	elect_position[4][2] = -4.956;
	elect_position[4][3] = -1.116;
	elect_position[5][1] = -10.500;
	elect_position[5][2] = -3.513;
	elect_position[5][3] = -3.670;
	elect_position[6][1] = -13.400;
	elect_position[6][2] = -4.956;
	elect_position[6][3] = -1.116;
	elect_position[7][1] = 4.763;
	elect_position[7][2] = 0.000;
	elect_position[7][3] = -2.330;
	elect_position[8][1] = 3.031;
	elect_position[8][2] = -1.950;
	elect_position[8][3] = -3.330;
	elect_position[9][1] = 3.031;
	elect_position[9][2] = 1.950;
	elect_position[9][3] = -3.330;
	elect_position[10][1] = 1.299;
	elect_position[10][2] = 0.000;
	elect_position[10][3] = -4.330;
	elect_position[11][1] = 0.993;
	elect_position[11][2] = 4.855;
	elect_position[11][3] = -1.116;
	elect_position[12][1] = -2.700;
	elect_position[12][2] = 3.513;
	elect_position[12][3] = -3.670;
	elect_position[13][1] = -6.200;
	elect_position[13][2] = 3.513;
	elect_position[13][3] = -3.670;
	elect_position[14][1] = -8.000;
	elect_position[14][2] = 4.956;
	elect_position[14][3] = -1.116;
	elect_position[15][1] = -10.500;
	elect_position[15][2] = 3.513;
	elect_position[15][3] = -3.670;
	elect_position[16][1] = -13.400;
	elect_position[16][2] = 4.956;
	elect_position[16][3] = -1.116;
	elect_position[17][1] = -2.800;
	elect_position[17][2] = 0.000;
	elect_position[17][3] = -5.080;
	elect_position[18][1] = -9.800;
	elect_position[18][2] = 0.000;
	elect_position[18][3] = -5.080;
	elect_position[19][1] = -13.600;
	elect_position[19][2] = 0.000;
	elect_position[19][3] = -5.080;

	elect_normal[1][1] = 0.196;
	elect_normal[1][2] = -0.956;
	elect_normal[1][3] = 0.220;
	elect_normal[2][1] = 0.0;
	elect_normal[2][2] = -0.692;
	elect_normal[2][3] = -0.722;
	elect_normal[3][1] = 0.0;
	elect_normal[3][2] = -0.692;
	elect_normal[3][3] = -0.722;
	elect_normal[4][1] = 0.0;
	elect_normal[4][2] = -0.976;
	elect_normal[4][3] = -0.220;
	elect_normal[5][1] = 0.0;
	elect_normal[5][2] = -0.976;
	elect_normal[5][3] = -0.220;
	elect_normal[6][1] = 0.5;
	elect_normal[6][2] = 0.0;
	elect_normal[6][3] = -0.866;
	elect_normal[7][1] = 0.5;
	elect_normal[7][2] = 0.0;
	elect_normal[7][3] = -0.866;
	elect_normal[8][1] = 0.5;
	elect_normal[8][2] = 0.0;
	elect_normal[8][3] = -0.866;
	elect_normal[9][1] = 0.5;
	elect_normal[9][2] = 0.0;
	elect_normal[9][3] = -0.866;
	elect_normal[10][1] = 0.5;
	elect_normal[10][2] = 0.0;
	elect_normal[10][3] = -0.866;
	elect_normal[11][1] = 0.196;
	elect_normal[11][2] = 0.956;
	elect_normal[11][3] = -0.220;
	elect_normal[12][1] = 0.0;
	elect_normal[12][2] = 0.692;
	elect_normal[12][3] = -0.722;
	elect_normal[13][1] = 0.0;
	elect_normal[13][2] = 0.692;
	elect_normal[13][3] = -0.722;
	elect_normal[14][1] = 0.0;
	elect_normal[14][2] = 0.976;
	elect_normal[14][3] = -0.220;
	elect_normal[15][1] = 0.0;
	elect_normal[15][2] = 0.692;
	elect_normal[15][3] = -0.722;
	elect_normal[16][1] = 0.0;
	elect_normal[16][2] = 0.976;
	elect_normal[16][3] = -0.220;
	elect_normal[17][1] = 0.0;
	elect_normal[17][2] = 0.0;
	elect_normal[17][3] = -1.000;
	elect_normal[18][1] = 0.0;
	elect_normal[18][2] = 0.0;
	elect_normal[18][3] = -1.000;
	elect_normal[19][1] = 0.0;
	elect_normal[19][2] = 0.0;
	elect_normal[19][3] = -1.000;

	mat_mult_scalar(elect_position, 0.001, elect_position);

	// Allocate memory for all of the vectors and matrices
	f_world_k_minus_1 = my_vector(1, 6);
	f_desired = my_vector(1, 6);
	Sigma_P = my_matrix(1, 6, 1, 6);
	Sigma_D = my_matrix(1, 6, 1, 6);
	f_diff_P = my_vector(1, 6);
	f_diff_D = my_vector(1, 6);
	X_dot_target = my_vector(1, 6);
	X_dot_P = my_vector(1, 6);
	X_dot_D = my_vector(1, 6);
	f_s = my_vector(1, 6);
	f_world = my_vector(1, 6);
	delta_O_target = my_vector(1, 3);
	force_sensed_w3 = my_vector(1,3);
	c_pos = my_vector(1,3);
	c_normal = my_vector(1,3);
	c_pos_world = my_vector(1,3);
	c_normal_world = my_vector(1,3);


	// Initialize values for the gain matrices
	for (r = 1; r <= 6; ++r)
	{
		for (c = 1; c <= 6; ++c)
		{
			Sigma_P[r][c] = 0.0;
			Sigma_D[r][c] = 0.0;
		}
	}

	for (i = 1; i <= 3; ++i)
	{
		Sigma_P[i][i] = sigma_P_f;
		Sigma_D[i][i] = sigma_D_f;
	}
	for (i = 4; i <= 6; ++i)
	{
		Sigma_P[i][i] = sigma_P_t;
		Sigma_D[i][i] = sigma_D_t;
	}

#ifdef DEBUG_PARAMETER_INIT

	printf("Sigma_P = \n");
	for (r = 1; r <= 6; ++r)
	{
		for (c = 1; c <= 6; ++c)
		{
			printf("%lf ", Sigma_P[r][c]);
		}
		printf("\n");
	}

	printf("\nSigma_D = \n");

	for (r = 1; r <= 6; ++r)
	{
		for (c = 1; c <= 6; ++c)
		{
			printf("%lf ", Sigma_D[r][c]);
		}
		printf("\n");
	}

	fflush(stdout);

#endif // DEBUG_PARAMETER_INIT

	u_force = my_vector(1,3);
	u_velocity = my_vector(1,3);
	u_hybrid = my_vector(1,3);
	v_d_k = my_vector(1,3);
	force_normal_desired = 0.0;
	force_contact_thresh = 0.0;
	// TODO: correctly set these gains
	pressure_delta = 0;//5e-5;
	pressure_Integral = 0;//5e-5;
	force_gain_I = 0;//5e-5;
	force_gain_P = 1e-3;
	pressure_gain_P = 1e-3;
	pressure_gain_D = 8e-3;
	pressure_gain_I = 1e-4;
	MAX_FORCE_INTEGRAL_MAGNITUDE = 2.0 / force_gain_I;
	// TODO: Set stopping criteria correctly
	DESIRED_FORCE_EPSILON = 0.01;
	desired_force_count_ = 0;
	controller_running_counter_ = 0;
	DESIRED_FORCE_COUNT_THRESH = 3;
	ENDEFFECTOR_MASS = 0.170; // in kg, mount mass without biotac
	ENDEFFECTOR_MASS += 0.016; // in kg, mass of nuts and bolts attaching mount to FT sensor
#ifdef BIOTAC_ATTACHED
	ENDEFFECTOR_MASS += 0.024; // in kg, biotac mass with bolts included
#endif // BIOTAC_ATTACHED

	// TODO: Initilize state estimation variables and set gains
	Gamma_r = my_matrix(1,3,1,3);
	// Strictly positive gains
	gamma_r = 0.8;
	beta_r = 0.1;
	gamma_n = 0.8;
	beta_n = 0.1;
	L_r_t =  my_matrix(1,3,1,3);
	r_hat_e_t = my_vector(1,3);
	c_r_t = my_vector(1,3);
	L_n_t = my_matrix(1,3,1,3);
	n_hat_c_t = my_vector(1,3);
	p_hat_c_t = my_vector(1,3);

	// NOTE: Needs to be -SPD matrix
	mat_eye(Gamma_r);
	mat_mult_scalar(Gamma_r, -gamma_r, Gamma_r);

	// More initialization
	P_bar = my_matrix(1,3,1,3);

	// Biotac stuff
	PDC_contact_threshold = 10;

	// transform functions
	I3 = my_matrix(1,3,1,3);
	a_mat = my_matrix(1,3,1,1);
	a_prod = my_matrix(1,3,1,3);
	mat_eye(I3);
	R_AB_trans = my_matrix(1, 3, 1, 3);
	transform_ll = my_matrix(1, 3, 1, 3);
	P_AB_hat_neg = my_matrix(1, 3, 1, 3);
	W_transform_AB = my_matrix(1, 6, 1, 6);
	R_sw = my_matrix(1, 3, 1, 3);
	R_ws = my_matrix(1, 3, 1, 3);
	P_sw = my_vector(1, 3);
	P_ws = my_vector(1, 3);

}

static int time_is_up();
static int time_is_up_base(int print_time);

float clip_magnitude(float val, float magnitude)
{
	return fmax(fmin(val, magnitude),-magnitude);
}
static double getTimeFloat()
{
	struct timeval cur_time_int;
	gettimeofday(&cur_time_int, NULL);
	double cur_time_flt = cur_time_int.tv_sec + 1e-6*cur_time_int.tv_usec;
	return cur_time_flt;
}

xmlrpc_value * run_force_control_callback(xmlrpc_double move_time, xmlrpc_env * const envP,
                                          int reset_sensor)
{
	static int i;
	des_force_move_time = move_time;
	printf("Des force move time %lf\n", des_force_move_time);
	fflush(stdout);
	controller_running_counter_ = 0;

	if (des_force_move_time > 0)
	{
		timing_force_move = 1;
		printf("Perfroming timed force move!\n");
		fflush(stdout);
	}
	else
	{
		fflush(stdout);
		timing_force_move = FALSE;
	}
	if (use_at_desired_force)
	{
		printf("Stopping on detected forces!\n");
		fflush(stdout);
	}
	else
	{
		fflush(stdout);
	}

	if (reset_sensor)
	{
		// printf("\nRDaneel F/T zeroing\n");
		// fflush(stdout);
		// zeroForceTorque();
		// printf("\nRDaneel sleeping\n");
		// fflush(stdout);
		// sleep(1);
		// printf("\nRDaneel F/T zeroed\n");
		// fflush(stdout);
		printf("FT sensor base = [");
		for(i = 1; i<=6; i++)
		{
			FTSenseBase[i] = misc_sensor[i]; //store the start value of the sensors
			printf("%lf ", misc_sensor[i]);
		}
		printf("]\n");
		fflush(stdout);
	}

	GotoState = EXERT_FORCE;
	sem_wait(&myservsem);

	return xmlrpc_build_value(envP,"b",1);
}

xmlrpc_value * PerformHybridControl(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
                          void * const serverInfo, void * const channelInfo)
{
	static int i;
	//set desired force
	static xmlrpc_double f_n_desired, v_x_desired, v_y_desired, v_z_desired, move_time;
	static xmlrpc_int stop_on_desired_force;
	printf("PerformHybridControl Callback!\n");
	fflush(stdout);

	xmlrpc_parse_value(envP, paramArrayP, "(dddddis#)",
		     	   &f_n_desired, &v_x_desired, &v_y_desired, &v_z_desired,
		     	   &move_time, &stop_on_desired_force,  &cur_log_name, &cur_log_name_length);

	printf("Parsed values!\n");
	fflush(stdout);
	use_at_desired_force = stop_on_desired_force;

	force_normal_desired = f_n_desired;
	v_d_k[1] = v_x_desired;
	v_d_k[2] = v_y_desired;
	v_d_k[3] = v_z_desired;
	current_controller = HYBRID_FORCE_CONTROL;

	// Reset state variables to initial values
	vec_zero(r_hat_e_t);
	mat_zero(L_r_t);
	vec_zero(c_r_t);
	mat_zero(L_n_t);
	vec_zero(p_hat_c_t);

	force_integral = 0.0;
	desired_force_count_ = 0;
	return run_force_control_callback(move_time, envP, FALSE);
}

xmlrpc_value * ExertForce(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
                          void * const serverInfo, void * const channelInfo)
{
	static int i;
	printf("ExertForce called\n");
	//set desired force
	static xmlrpc_double ForceX, ForceY, ForceZ, TorqueX, TorqueY, TorqueZ, move_time;
	static xmlrpc_int stop_on_desired_force;
	xmlrpc_parse_value(envP, paramArrayP, "(dddddddi)",
		     	   &ForceX, &ForceY, &ForceZ,
		     	   &TorqueX, &TorqueY, &TorqueZ,
		     	   &move_time, &stop_on_desired_force);
	use_at_desired_force = stop_on_desired_force;
	current_controller = FORCE_FEEDBACK_CONTROL;
	DesForce[1] = ForceX;
	DesForce[2] = ForceY;
	DesForce[3] = ForceZ;
	DesForce[4] = TorqueX;
	DesForce[5] = TorqueY;
	DesForce[6] = TorqueZ;

	for (i = 1; i <= 6; ++i)
	{
		f_desired[i] = DesForce[i];
	}
	printf("Desired forces %lf %lf %lf\n", DesForce[1], DesForce[2], DesForce[3]);
	printf("Desired torques %lf %lf %lf\n", DesForce[4], DesForce[5], DesForce[6]);
	return run_force_control_callback(move_time, envP, TRUE);
}

xmlrpc_value * MoveUntilContact(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
                               void * const serverInfo, void * const channelInfo)
{
	static int i;
	//set desired force
	static xmlrpc_double contact_thresh, v_x_desired, v_y_desired, v_z_desired, move_time;
	static xmlrpc_int use_biotac;
	xmlrpc_parse_value(envP, paramArrayP, "(dddddis#)",
		     	   &contact_thresh, &v_x_desired, &v_y_desired, &v_z_desired,
		     	   &move_time, &use_biotac, &cur_log_name, &cur_log_name_length);

	v_d_k[1] = v_x_desired;
	v_d_k[2] = v_y_desired;
	v_d_k[3] = v_z_desired;
	if (use_biotac)
	{
		current_controller = BIOTAC_THRESHOLD_CONTROL;
		PDC_contact_threshold = contact_thresh;
		printf("Requesting biotac for contact detection\n");
	}
	else
	{
		current_controller = FORCE_THRESHOLD_CONTROL;
		force_contact_thresh = contact_thresh;
		printf("Requesting ft for contact detection\n");
	}
	fflush(stdout);
	use_at_desired_force = FALSE;
	force_integral = 0.0;
	desired_force_count_ = 0;
	return run_force_control_callback(move_time, envP, TRUE);
}

xmlrpc_value * MoveTaskVelocity(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
                                void * const serverInfo, void * const channelInfo)
{
	static int i;
	//set desired force
	static xmlrpc_double v_x_desired, v_y_desired, v_z_desired, move_time;
	xmlrpc_parse_value(envP, paramArrayP, "(dddds#)", &v_x_desired, &v_y_desired, &v_z_desired, &move_time,
			   &cur_log_name, &cur_log_name_length);

	v_d_k[1] = v_x_desired;
	v_d_k[2] = v_y_desired;
	v_d_k[3] = v_z_desired;
	printf("V_desired: (%lf, %lf %lf)\n", v_d_k[1], v_d_k[2], v_d_k[3]);
	printf("move time: %lf\n", move_time);
	printf("Log name is: %s!\n\n",cur_log_name);
	current_controller = MOVE_TASK_VELOCITY_CONTROL;
	use_at_desired_force = FALSE;
	force_integral = 0.0;
	desired_force_count_ = 0;
	return run_force_control_callback(move_time, envP, FALSE);
}

xmlrpc_value * CollectBiotacData(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
                                 void * const serverInfo, void * const channelInfo)
{
	Cheetah ch_handle;
	bt_info biotac;
	// double electrode_data[19];
	// double pac_data[22];
	// double pdc_data;
	// double tac_data;
	// double tdc_data;
	double local_misc_sensor[N_MISC_SENSORS+1];
	int num_biotacs = initialize_biotacs(&ch_handle, &biotac);
	printf("Initialized %i biotacs\n", num_biotacs);
	int i,j;
	int num_runs = 20;
	if (num_biotacs > 0)
	{
		for (i = 0; i < num_runs; ++i)
		{
			// get_latest_biotac_data(&ch_handle, &biotac, electrode_data, pac_data, &pdc_data, &tac_data, &tdc_data);
			get_latest_biotac_data(&ch_handle, &biotac, local_misc_sensor+bt_E01,local_misc_sensor+bt_PAC01,
					       local_misc_sensor+bt_PDC, local_misc_sensor+bt_TAC, local_misc_sensor+bt_TDC);
			printf("Collected data for call %i\n", i);
			for (j = 0; j < 19; ++j)
			{
				printf("E%.2d = %6lf\n", j, local_misc_sensor[bt_E01+j]);
			}
			for (j = 0; j < 22; ++j)
			{
				printf("PAC%.2d = %6lf\n", j, local_misc_sensor[bt_PAC01+j]);
			}
			printf("TAC = %6lf\n", local_misc_sensor[bt_TAC]);
			printf("TDC = %6lf\n", local_misc_sensor[bt_TDC]);
			printf("PDC = %6lf\n", local_misc_sensor[bt_PDC]);
		}
	}
	close_biotac(&ch_handle);
	return xmlrpc_build_value(envP, "b", num_biotacs);
}

xmlrpc_value * slip_control(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
                          void * const serverInfo, void * const channelInfo)
{

	size_t tt = 0;
	size_t ct = 0;
	size_t tn = 0;
	size_t tt2 = 0;
	size_t tn2 = 0;
	size_t tt3 = 0;
	size_t ft = 0;
	size_t pt = 0;

	static xmlrpc_double contact_thresh, v_x_desired, v_y_desired, v_z_desired, move_time, mode;

	xmlrpc_parse_value(envP, paramArrayP, "(ddddds#s#s#s#s#s#s#s#ds#)", &contact_thresh, &v_x_desired, &v_y_desired, &v_z_desired, &mode,
			   &training_type, &tt, &classifier_type, &ct, &train_names, &tn, &train_trials, &tt2, &target_name, &tn2,
			   &target_trials, &tt3, &feature_type, &ft, &prediction_type, &pt, &move_time, &cur_log_name, &cur_log_name_length);

	v_d_k[1] = v_x_desired;
	v_d_k[2] = v_y_desired;
	v_d_k[3] = v_z_desired;

	PDC_contact_threshold = contact_thresh;
	class_mode_select = mode;
	current_controller = SLIP_CONTROL;
	use_at_desired_force = FALSE;
	force_integral = 0.0;
	desired_force_count_ = 0;

	use_at_desired_force = FALSE;

	return run_force_control_callback(move_time, envP, FALSE);
}

xmlrpc_value * slip_pinch_control(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
                          void * const serverInfo, void * const channelInfo)
{
	size_t tt = 0;
	size_t ct = 0;
	size_t tn = 0;
	size_t tt2 = 0;
	size_t tn2 = 0;
	size_t tt3 = 0;
	size_t ft = 0;
	size_t pt = 0;

	static xmlrpc_double contact_thresh, v_x_desired, v_y_desired, v_z_desired, move_time, mode;

	xmlrpc_parse_value(envP, paramArrayP, "(ddddds#s#s#s#s#s#s#s#ds#)", &contact_thresh, &v_x_desired, &v_y_desired, &v_z_desired, &mode,
			   &training_type, &tt, &classifier_type, &ct, &train_names, &tn, &train_trials, &tt2, &target_name, &tn2,
			   &target_trials, &tt3, &feature_type, &ft, &prediction_type, &pt, &move_time, &cur_log_name, &cur_log_name_length);

	v_d_k[1] = v_x_desired;
	v_d_k[2] = v_y_desired;
	v_d_k[3] = v_z_desired;

	PDC_contact_threshold = contact_thresh;
	class_mode_select = mode;
	current_controller = SLIP_PINCH_CONTROL;
	use_at_desired_force = FALSE;
	force_integral = 0.0;
	desired_force_count_ = 0;

	use_at_desired_force = FALSE;
	return run_force_control_callback(move_time, envP, FALSE);
}

xmlrpc_value * slip_data_collection(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
			void * const serverInfo, void * const channelInfo)
{

	static xmlrpc_double contact_thresh, v_x_desired, v_y_desired, v_z_desired, move_time;
	size_t et = 0;
	xmlrpc_parse_value(envP, paramArrayP, "(ddddds#s#)",
			   &contact_thresh, &v_x_desired, &v_y_desired, &v_z_desired,
			   &move_time, &exp_type, &et, &cur_log_name, &cur_log_name_length);

	force_normal_desired = contact_thresh;

	v_d_k[1] = v_x_desired;
	v_d_k[2] = v_y_desired;
	v_d_k[3] = v_z_desired;

	//PDC_contact_threshold = contact_thresh;

	current_controller = SLIP_DATA_COLLECTION;
	use_at_desired_force = FALSE;
	desired_force_count_ = 0;

	return run_force_control_callback(move_time, envP, FALSE);
}

xmlrpc_value * validate_classifier(xmlrpc_env * const envP,xmlrpc_value * const paramArrayP,
			void * const serverInfo, void * const channelInfo)
{
	size_t tt = 0;
	size_t ct = 0;
	size_t tn = 0;
	size_t tt2 = 0;
	size_t tn2 = 0;
	size_t tt3 = 0;
	size_t ft = 0;
	size_t pt = 0;

	static xmlrpc_double move_time;
	size_t et = 0;

	xmlrpc_parse_value(envP, paramArrayP, "(s#s#s#s#s#s#s#s#ds#)", &training_type, &tt, &classifier_type,
			   &ct, &train_names, &tn, &train_trials, &tt2, &target_name, &tn2, &target_trials, &tt3,
			   &feature_type, &ft, &prediction_type, &pt, &move_time, &cur_log_name, &cur_log_name_length);

	//PDC_contact_threshold = contact_thresh;

	current_controller = CLASSIFIER_VALIDATION;
	use_at_desired_force = FALSE;
	desired_force_count_ = 0;

	return run_force_control_callback(move_time, envP, FALSE);
}


int do_slip_control(Vector X_dot)
{

	int i;

	double PDC_diff = misc_sensor[bt_PDC] - PDC_tare;

	get_contact_position(c_pos_world);
	get_contact_normal(c_normal_world);
	printf("PDC: %f\t Label : %d\n PDC_good: %f\t", PDC_diff, Class_value, PDC_good);
	if (PDC_good == 0) {
		inital_stability = 50;
		PDC_good = 100;
		stability_filter = 0;
	}
	if(PDC_diff > PDC_good+50){
		inital_stability = 50;
		/*for (i = 1; i <= 3; ++i)
		{
				X_dot[i] = -(c_normal_world[i]*c_response);
		}*/
		X_dot[2]= 0.02;//c_response;
		if(c_response < 0.50){
			c_response = c_response + 0.01;
		}


	}else if (Class_value == -1 && PDC_diff < PDC_good+50){

		stability_filter++;

		for (i = 1; i <= 3; ++i)
		{
			//X_dot[i] = 0.0;//(c_normal_world[i]*c_response);
		/*	if (i != 2){
				X_dot[i] = -(c_normal_world[i]*c_response);
			}else{
				X_dot[i] = (c_normal_world[i]*c_response);
			}*/
		}
		if (stability_filter>5){
			X_dot[2]=-c_response;
		}else{
			inital_stability--;
		}
		if(c_response < 1.0){
			c_response = c_response + 0.01;
		}


	/*}else if (Class_value == -1 && PDC_diff <= 6){

		for (i = 1; i <= 3; ++i)
		{
			X_dot[i] = 0.0;
		}

*/
	}else if (Class_value == 0){

		inital_stability--;
		stability_filter = 0;

		for (i = 1; i <= 3; ++i)
		{
			X_dot[i] = 0.0;
		}

		if(inital_stability<=0 && PDC_good == 150){
			inital_stability = 200;
			PDC_good = PDC_diff;
		}

	}

	reset_random_vel = 1;



	for (i = 4; i <= 6; ++i)
	{
		X_dot[i] = 0.0;
	}

	double testdouble[6];
	memcpy(testdouble,c_pos_world+1,sizeof(double)*3);
	memcpy(testdouble+3,c_normal_world+1,sizeof(double)*3);
	sendUserGraphics("disppose2",testdouble, 6*sizeof(double));

	double testdouble2[7];
	memcpy(testdouble2,cart_des_state[RIGHT_HAND].x+1,sizeof(double)*3);
	memcpy(testdouble2+3,cart_des_orient[RIGHT_HAND].q+1,sizeof(double)*4);
	sendUserGraphics("disppose",testdouble2, 7*sizeof(double));

	return 0;
}

void get_contact_position(Vector contact_position){

	int i,j;
	int num_electrodes = 19;

	double r = 0.008;
	double elec_sum = 0.0;
	double v_norm = 0.0;
	double n_norm = 0.0;
	double electrode_data[num_electrodes];

	double xAxis[3];
	double yAxis[3];
	double zAxis[3];

	Matrix Rot_Matrix;
	Rot_Matrix = my_matrix(1,3,1,3);

	Vector aux;
	aux = my_vector(1,3);

	Vector weights;
	weights = my_vector(1,num_electrodes);


	for (j = 0; j < num_electrodes; ++j){
		electrode_data[j] = misc_sensor[bt_E01+j]-electrode_tare[j];
		weights[j+1] = pow(electrode_data[j],2);
		elec_sum += weights[j+1];
	}

	mat_sum_rows_weighted(elect_position, weights, contact_position);

	vec_mult_scalar(contact_position, 1.0/elec_sum, contact_position);

	if (contact_position[1] > 0){

		for (j = 1; j <= 3; ++j)
		{
			v_norm += pow(contact_position[j],2);
		}

		vec_mult_scalar(contact_position, r/sqrt(v_norm), contact_position);

	}else{

		for (j = 2; j <= 3; ++j)
		{
			v_norm += pow(contact_position[j],2);
		}
		for (j = 2; j <= 3; ++j)
		{
			contact_position[j] = contact_position[j]*(r/sqrt(v_norm));
		}

	}

	for (j = 1; j <= 3; ++j)
	{
		c_pos[j] = contact_position[j];
	}

	quatToBasisVectors(cart_orient[RIGHT_HAND].q+1, xAxis, yAxis, zAxis);

	Rot_Matrix[1][1] =  xAxis[0];
	Rot_Matrix[1][2] =  xAxis[1];
	Rot_Matrix[1][3] =  xAxis[2];
	Rot_Matrix[2][1] =  yAxis[0];
	Rot_Matrix[2][2] =  yAxis[1];
	Rot_Matrix[2][3] =  yAxis[2];
	Rot_Matrix[3][1] =  zAxis[0];
	Rot_Matrix[3][2] =  zAxis[1];
	Rot_Matrix[3][3] =  zAxis[2];

	for (j = 1; j <= 3; ++j)
	{
		aux[j] = cart_state[RIGHT_HAND].x[j];
	}

	mat_vec_mult(Rot_Matrix, contact_position, contact_position);

	vec_add(aux, contact_position, contact_position);


}

void get_contact_normal(Vector contact_normal){

	int i,j;
	int num_electrodes = 19;

	double r = 8.0;
	double elec_sum = 0.0;
	double v_norm = 0.0;
	double n_norm = 0.0;
	double electrode_data[num_electrodes];

	double xAxis[3];
	double yAxis[3];
	double zAxis[3];

	Matrix Rot_Matrix;
	Rot_Matrix = my_matrix(1,3,1,3);

	Vector weights;
	weights = my_vector(1,num_electrodes);


	for (j = 0; j < num_electrodes; ++j){
		electrode_data[j] = misc_sensor[bt_E01+j]-electrode_tare[j];
		weights[j+1] = pow(electrode_data[j],2);
		elec_sum += weights[j+1];
	}

	mat_sum_rows_weighted(elect_normal, weights, contact_normal);

	for (j = 1; j <= 3; ++j)
	{
		n_norm += pow(contact_normal[j],2);
	}

	vec_mult_scalar(contact_normal, 1.0/sqrt(n_norm), contact_normal);

	//TODO: Change tranformation method.

	for (j = 1; j <= 3; ++j)
	{
		c_normal[j] = contact_normal[j];
	}

	quatToBasisVectors(cart_des_orient[RIGHT_HAND].q+1, xAxis, yAxis, zAxis);

	Rot_Matrix[1][1] =  xAxis[0];
	Rot_Matrix[1][2] =  xAxis[1];
	Rot_Matrix[1][3] =  xAxis[2];
	Rot_Matrix[2][1] =  yAxis[0];
	Rot_Matrix[2][2] =  yAxis[1];
	Rot_Matrix[2][3] =  yAxis[2];
	Rot_Matrix[3][1] =  zAxis[0];
	Rot_Matrix[3][2] =  zAxis[1];
	Rot_Matrix[3][3] =  zAxis[2];

	mat_vec_mult(Rot_Matrix, contact_normal, contact_normal);

}

int init_slip_control()
{

	char test[] = "Position";
	char test2[] = "Normal";
	int i,j;
	int num_runs = 2;
	int firstiter = 0;
	int num_electrodes = 19;

	double r = 8.0;
	double elec_sum = 0.0;
	double v_norm = 0.0;
	double n_norm = 0.0;
	double electrode_data[num_electrodes];

	Vector weights;
	weights = my_vector(1,num_electrodes);
	//  Vector c_pos;
	//  c_pos = my_vector(1,3);

	Vector tar_c_pos;
	tar_c_pos = my_vector(1,3);

	//  Vector c_normal;
	//  c_normal = my_vector(1,3);


	Vector c_normal_final;
	c_normal_final = my_vector(1,3);

	for (j = 0; j < num_electrodes; ++j)
	{
		electrode_data[j] = misc_sensor[bt_E01+j];
	}

	for (j = 0; j < num_electrodes; ++j)
	{
		weights[j+1] = pow(electrode_data[j]-electrode_tare[j],2);
	}



	for (j = 0; j < num_electrodes; ++j)
	{
		elec_sum += weights[j+1];
	}

	mat_sum_rows_weighted(elect_position, weights, c_pos);
	mat_sum_rows_weighted(elect_normal, weights, c_normal);


	vec_mult_scalar(c_pos, 1.0/elec_sum, c_pos);

	if (c_pos[1] > 0){

		for (j = 1; j <= 3; ++j)
		{
			v_norm += pow(c_pos[j],2);
		}

		vec_mult_scalar(c_pos, r/sqrt(v_norm), c_pos);

	}else{

		for (j = 2; j <= 3; ++j)
		{
			v_norm += pow(c_pos[j],2);
		}
		for (j = 2; j <= 3; ++j)
		{
			c_pos[j] = c_pos[j]*(r/sqrt(v_norm));
		}


	}

	if (elec_sum != 0){
		vec_add(c_pos_tare, c_pos, c_pos_tare);
		vec_add(c_normal_tare, c_normal, c_normal_tare);

		if(c_pos_tare_init == 19){
			vec_mult_scalar(c_pos_tare, 0.05, c_pos_tare);
			vec_mult_scalar(c_normal_tare, 0.05, c_normal_tare);
		}

		c_pos_tare_init++;
	}

	return 0;
}

void do_exert_force(int* stateptr, int* firsttimeptr)
{
	int file_number;
	char sl_filename[100];
	FILE* log_key_file;

	static int i, r, c;
	float force_sum;
	int should_stop;
	should_stop = 0;
	pthread_t rdf_id;
	int rdgen = 1;
	double displacement = 0;
	double Fxy_norm = 0;
	double Fx, Fy, Tz;
	double c_time = 0;
	double exp_time = 0;
	Vector aux_v;


	// get desired force to apply
	// get current force
	// calculate cartesian from force
	// cartesian position -> store in global var
	// cart2jointstate
	if(*firsttimeptr==1)
	{
		//printf("\n\rEXERT FORCE\n\r");
		//fflush(stdout);

		setWtoMinJerk();

		// Setting task start time
		force_move_start_time = getTimeFloat();

#ifdef DEBUG_FORCES_START
		printf("Got start time of: %lf\n", force_move_start_time);
		printf("Sensor: \t");
		for (i = 1; i <= 6; ++i)
		{
			printf("%lf ", misc_sensor[i]);
		}
		printf("\n");
		printf("Sensor baseline: ");
		for (i = 1; i <= 6; ++i)
		{
			printf("%lf ", FTSenseBase[i]);
		}
		printf("\n");
		printf("Desired forces: ");
		for (i = 1; i <= 6; ++i)
		{
			printf("%lf ", DesForce[i]);
		}
		printf("\n");
		printf("Des force move time %lf\n\n", des_force_move_time);
		fflush(stdout);
#endif // DEBUG_FORCES_START

		// changeCollectFreq(10);
		stopcd();
		scd();
	}

	// Sensed forces and torques
	for(i = 1; i <= 6; i++)
	{
		// Remove initial values to compensate for gravity, assumes we aren't changing EE orientation
		f_s[i] = (misc_sensor[i] - FTSenseBase[i]);
	}

	ft_sensor_wrench_in_world_frame(f_s, f_world);

#ifdef DEBUG_FORCES
	printf("\nraw force in sensor space = [%lf %lf %lf]\n", misc_sensor[1], misc_sensor[2], misc_sensor[3]);
	printf("base force in sensor space = [%lf %lf %lf]\n", FTSenseBase[1], FTSenseBase[2], FTSenseBase[3]);
	printf("zeored force in sensor space = [%lf %lf %lf]\n", f_s[1], f_s[2], f_s[3]);
	printf("force in world frame = [%lf %lf %lf]\n", f_world[1], f_world[2],f_world[3]);
	fflush(stdout);
#endif // DEBUG_FORCES

	// Remove forces caused by acceleration of the EE mass
	for (i = 1; i <=3; ++i)
	{
#ifdef DEBUG_FORCES
		printf("EE mass forces[%i] = %lf\n", i, ENDEFFECTOR_MASS*cart_state[1].xdd[i]);
#endif // DEBUG_FORCES
		// f_world[i] -= ENDEFFECTOR_MASS*cart_state[1].xdd[i];
	}

#ifdef DEBUG_FORCES
	printf("Forces with motion removed = [%lf, %lf, %lf]\n\n", f_world[1], f_world[2], f_world[3]);
	fflush(stdout);
#endif

	// For the first time set the values the same for damping
	if(*firsttimeptr==1)
	{
		*firsttimeptr=0;
		for (i = 1; i <= 6; i++)
		{
			f_world_k_minus_1[i] = f_world[i];
		}
		if (current_controller == HYBRID_FORCE_CONTROL)
		{
			// Initialize n_hat_c_t = f(0)/||f(0)||
			force_sum = 0.0;
			for (i = 1; i <= 3; ++i)
			{
				force_sum += fabs(f_world[i]);
			}
			if (force_sum <= 0.0)
			{
				force_sum = 1.0;
			}
			for (i = 1; i <= 3; ++i)
			{
				n_hat_c_t[i] = f_world[i]/force_sum;
			}
		}

		// NOTE: Add any other biotac based controllers to this check so that we don't try and use feedback when
		// the sensor isn't working
		if (current_controller == BIOTAC_THRESHOLD_CONTROL || (current_controller == SLIP_CONTROL && class_mode_select != 0))
		{
			// Quit out if the biotac isn't working and we need it for control
			if(!biotac_working())
			{
				current_controller = CANCEL_FORCE_CONTROL;
				printf("Biotac not working! Cancelling!\n");
				fflush(stdout);
			}
		}

		// Tare Biotac
		// TODO: Move into function
		// TODO: do for other sensors
		//PDC_tare = misc_sensor[bt_PDC];

		//for (i = 0; i < 19; ++i)
		//{
		//   electrode_tare[i] += misc_sensor[bt_E01+i];
		//}

		c_pos_tare = my_vector(1,3);
		vec_zero(c_pos_tare);

		c_normal_tare = my_vector(1,3);
		vec_zero(c_normal_tare);
		PDC_good = 0;
		if(!classifier_active && (current_controller == SLIP_PINCH_CONTROL || current_controller == SLIP_CONTROL || current_controller == CLASSIFIER_VALIDATION)){

			//printf("Setting Arguments\n");
			args.mode = class_mode_select;
			args.training_type = training_type;;
			args.classifier_type = classifier_type;
			args.train_names = train_names;
			args.train_trials = train_trials;
			args.target_name = target_name;
			args.target_trials = target_trials;
			args.feature_type = feature_type;
			args.prediction_type = prediction_type;
			args.sensor_pointer = misc_sensor+bt_E01;

			/*printf("%d\n", args.mode);
			printf("%s\n", args.training_type);
			printf("%s\n", args.train_names);
			printf("%s\n", args.train_trials);
			printf("%s\n", args.target_name);
			printf("%s\n", args.target_trials);
			printf("%s\n", args.feature_type);
			printf("%s\n", args.prediction_type);

			printf("Thread Lauching\n");*/
			if(!thread_launched){
			pthread_create(&(rdf_id), NULL, &online_classification, (void *)&args);
			thread_launched = 1;
			}
			//printf("AFTER PTHREAD\n");

			classifier_active = 1;
			Class_Trained = 0;
		}

		c_pos_tare_init = 0;
		in_contact = 0;
		stability_counter = 0;
		inital_stability = 0;
		has_moved_back = 0;
		reset_random_vel = 1;
		joint_position_control = 0;
		init_pos_reached=0;

		slip_dc_init_pos[0] = 0.514;
		slip_dc_init_pos[1] = 0.195;
		slip_dc_init_pos[2] = 0.352;
		slip_dc_init_pos[3] = 0.710;
		slip_dc_init_pos[4] = 0.210;
		slip_dc_init_pos[5] = 0.175;
		slip_dc_init_pos[6] = -0.649;

		random_pos_init(slip_dc_init_pos);
		dmp_joint_init(slip_dc_init_pos);
		ext_x = 0;
		ext_y = 0;

	}

	controller_running_counter_ += 1;
	int s_test_value;

	//  printf("TARE: %f\n", PDC_tare);
	//  printf("Value: %f\n", misc_sensor[bt_PDC]);


	switch (current_controller)
	{
		case FORCE_FEEDBACK_CONTROL:
			// TODO: Make this use the corrected force from above
			should_stop = force_feedback_control(X_dot_target, firsttimeptr);
			break;

		case HYBRID_FORCE_CONTROL:
			update_contact_loc_estimate(r_hat_e_t, L_r_t, c_r_t, p_hat_c_t, f_s);
			update_contact_normal_estimate(n_hat_c_t, L_n_t);
			// TODO: Create method here to modify desired velocity in hybrid force control (update v_d_k)
			should_stop = hybrid_force_velocity_control(v_d_k, force_normal_desired, n_hat_c_t,
								    f_world, X_dot_target);
			break;

		case FORCE_THRESHOLD_CONTROL:
			should_stop = move_until_contact_control(v_d_k, force_contact_thresh, f_world,
								 X_dot_target);
			break;

		case BIOTAC_THRESHOLD_CONTROL:
			should_stop = move_until_contact_biotac_control(v_d_k, PDC_contact_threshold, X_dot_target);
			break;

		case MOVE_TASK_VELOCITY_CONTROL:
			should_stop = move_task_velocity_control(v_d_k, X_dot_target);
			break;

		case SLIP_DATA_COLLECTION:

			//if(joint_position_control == 0){
			//	joint_position_control = 1;
			//}
			//should_stop = go_to_joint_config();
			should_stop = collect_slip_data();
			break;

		case SLIP_CONTROL:

			if(Class_Trained){

				if (class_mode_select != 2){
					//printf("IN IF SHOULD STOP!!");
					should_stop = 1;
				}else if (in_contact){
						should_stop = do_slip_control(X_dot_target);
						should_stop = 0;
					}else{
						in_contact = move_until_contact_biotac_control(v_d_k, PDC_contact_threshold, X_dot_target);

					}
			}
			break;

		case SLIP_PINCH_CONTROL:

			if(Class_Trained){
				c_time = getTimeFloat();
				exp_time = c_time - slip_start_time;

				if (class_mode_select == 0){
					should_stop = 1;
				}else if(in_contact && exp_time > 5.0){

					if (Class_value || inital_stability > 0){
						should_stop = do_slip_control(X_dot_target);
					}else{
						if(reset_random_vel){
							v_d_k[1] = (my_ran0(&rdgen)-0.5)*0.1;
							v_d_k[2] = my_ran0(&rdgen)*0.05+0.05;
							v_d_k[3] = (my_ran0(&rdgen)-0.5)*0.1;
							reset_random_vel = 0;
						}
						should_stop = move_task_velocity_control(v_d_k, X_dot_target);
						has_moved_back = 1;
					}


					if(exp_time > 15.0){
						printf("Test Time expired : %f\n", exp_time);
						should_stop = 1;
					}
				}else if(!in_contact){
					in_contact = move_until_contact_biotac_control(v_d_k, PDC_contact_threshold, X_dot_target);
					if(in_contact){
						slip_start_time = getTimeFloat();
					}
				}
			}

			break;

		case CLASSIFIER_VALIDATION:
			if(Class_Trained){
				should_stop = 1;
			}
			break;

		case CANCEL_FORCE_CONTROL:

		default:
			printf("Canceling control!\n");
			fflush(stdout);
			should_stop = TRUE;
	}

	if( (timing_force_move && time_is_up()) || should_stop )
	{
		if (should_stop)
		{
			printf("Stopping at request of controller\n");
		}
		else
		{
			printf("Time is up!\n");
		}
		time_is_up_base(TRUE);
		// Save data to disk here
		stopcd();
		// Write the current log name to a key file to associate with the SL saved log file.
		file_number = saveData();
		if (file_number >= 0)
		{
			sprintf(sl_filename, "d%05d", file_number);
		}
		if (cur_log_name_length > 0)
		{
			printf("%s associated with sl file %s\n", cur_log_name, sl_filename);
			log_key_file = fopen(log_key_file_name, "a");
			fprintf(log_key_file, "%s %s\n", cur_log_name, sl_filename);
			fclose(log_key_file);
		}
		// Stop moving / exit task
		*stateptr = AWAITING_ORDERS;
		GotoState = AWAITING_ORDERS;
		Register_Idle = 1;
		*firsttimeptr = 1;

		// TODO: Move into its own method for reuse
		//stop:set desired to current
		ddmp_reset(1, cart_state[1].x[_X_],cart_state[1].x[_X_],0);
		ddmp_reset(2, cart_state[1].x[_Y_],cart_state[1].x[_Y_],0);
		ddmp_reset(3, cart_state[1].x[_Z_],cart_state[1].x[_Z_],0);
		ddmp_reset(4, cart_orient[1].q[_Q0_],cart_orient[1].q[_Q0_],0);
		ddmp_reset(5, cart_orient[1].q[_Q1_],cart_orient[1].q[_Q1_],0);
		ddmp_reset(6, cart_orient[1].q[_Q2_],cart_orient[1].q[_Q2_],0);
		ddmp_reset(7, cart_orient[1].q[_Q3_],cart_orient[1].q[_Q3_],0);
		Tau = 8.0;
		ddmp_run_x_all(1, Tau, time_step);
		set_cartesian_from_dmps();
		sem_post(&myservsem);
	}
	else
	{
	// TODO: Periodically save stuff to disk.
	}

	// Send commands to the robot
	if(joint_position_control == 0){
		jacobian_inv_control(X_dot_target, stateptr, firsttimeptr);
	}
}

/////////////////////////////////////////////////////////////////////////////////////
// Control functions
/////////////////////////////////////////////////////////////////////////////////////

int collect_slip_data(){


	int should_stop = 0;
	int rdgen = 1;
	double c_time = 0;
	double exp_time = 0;

	Vector n_v_d_k;
	n_v_d_k = my_vector(1,3);
	get_contact_position(c_pos_world);
	get_contact_normal(c_normal_world);

	if(init_pos_reached==0){

		if(joint_position_control == 0){
			joint_position_control = 1;
		}
		init_pos_reached = go_to_joint_config();
	}else{

		if(in_contact){
			c_time = getTimeFloat();

			exp_time = c_time - slip_start_time;

			if(exp_time < 0.15/sqrt(sqr(v_d_k[1]) + sqr(v_d_k[2]))){

				joint_position_control = 0;
				if(strcmp(exp_type,"h") == 0){
					n_v_d_k[1] = v_d_k[1];
					n_v_d_k[2] = v_d_k[2];
					n_v_d_k[3] = 0;
				}else if(strcmp(exp_type,"hs") == 0){
					n_v_d_k[1] = 0;
					n_v_d_k[2] = 0;
					n_v_d_k[3] = 0;
				}else{
					n_v_d_k[1] = v_d_k[1];
					n_v_d_k[2] = 0;
					n_v_d_k[3] = v_d_k[2];
				}
				if(strcmp(exp_type,"hs") != 0){
					should_stop = hybrid_bt_pressure_velocity_control(n_v_d_k, force_normal_desired, c_normal_world, X_dot_target);
				}
			}else{

				if(joint_position_control==0){
					joint_position_control = 1;
					dmp_joint_init(slip_dc_init_pos);
				}
				should_stop = go_to_joint_config();
			}
		}else{
			joint_position_control = 0;
			n_v_d_k[1] = 0;
			if(strcmp(exp_type,"h") == 0 || strcmp(exp_type,"hs") == 0){
				n_v_d_k[2] = 0;
				n_v_d_k[3] = -v_d_k[3];
			}else{
				n_v_d_k[2] = -v_d_k[3];
				n_v_d_k[3] = 0;
			}
			in_contact = move_until_contact_biotac_control(n_v_d_k, 5, X_dot_target);
			if(in_contact){
				slip_start_time = getTimeFloat();
				//force_normal_desired = force_normal_desired-5+misc_sensor[bt_PDC]-PDC_tare;

				do{
					v_d_k[1] = my_gasdev(&rdgen)*sqrt(0.05);
				}while(v_d_k[1]> 0.05 || v_d_k[1] < -0.05);
				do{
					v_d_k[2] = my_gasdev(&rdgen)*sqrt(0.05);
				}while(v_d_k[2]> 0.05 || v_d_k[2] < -0.05);
			}
		}

	}

	return should_stop;
}
void random_pos_init(double *target){

	int rdgen = 1;
	double t1, t2, d1, d2, d3;
	double temp_target_orientation[4];
	double target_orientation[4];
	Matrix x_Rot, y_Rot, z_Rot, f_Rot;
	x_Rot = my_matrix(1,3,1,3);
	y_Rot = my_matrix(1,3,1,3);
	z_Rot = my_matrix(1,3,1,3);
	f_Rot = my_matrix(1,3,1,3);
	double aux[3][3];

	temp_target_orientation[0] = target[3];
	temp_target_orientation[1] = target[4];
	temp_target_orientation[2] = target[5];
	temp_target_orientation[3] = target[6];

	do{
		t1 = my_gasdev(&rdgen)*0.01;
	}while(t1> 0.01 || t1 < -0.01);

	do{
		t2 = my_gasdev(&rdgen)*0.01;
	}while(t2> 0.01 || t2 < -0.01);

	do{
		d1 = my_gasdev(&rdgen)*0.1-0.15;
	}while(d1> -0.05 || d1 < -0.25);

	do{
		d2 = my_gasdev(&rdgen)*0.25;
	}while(d2 > 0.25 || d2 < -0.25);

	do{
		d3 = my_gasdev(&rdgen)*3.14;
	}while(d3> 3.14 || d3 < -3.14);

    	if(strcmp(exp_type,"v") == 0){
		d1 = d1 +1.570796;
	}

	x_Rot[1][1] = 1;
	x_Rot[1][2] = 0;
	x_Rot[1][3] = 0;
	x_Rot[2][1] = 0;
	x_Rot[2][2] = cos(d1);
	x_Rot[2][3] = -sin(d1);
	x_Rot[3][1] = 0;
	x_Rot[3][2] = sin(d1);
	x_Rot[3][3] = cos(d1);

    	if(strcmp(exp_type,"v") == 0){

		y_Rot[1][1] = cos(d3);
		y_Rot[1][2] = 0;
		y_Rot[1][3] = sin(d3);
		y_Rot[2][1] = 0;
		y_Rot[2][2] = 1;
		y_Rot[2][3] = 0;
		y_Rot[3][1] = -sin(d3);
		y_Rot[3][2] = 0;
		y_Rot[3][3] = cos(d3);

		z_Rot[1][1] = cos(d2);
		z_Rot[1][2] = -sin(d2);
		z_Rot[1][3] = 0;
		z_Rot[2][1] = sin(d2);
		z_Rot[2][2] = cos(d2);
		z_Rot[2][3] = 0;
		z_Rot[3][1] = 0;
		z_Rot[3][2] = 0;
		z_Rot[3][3] = 1;


	mat_mult(x_Rot, y_Rot, y_Rot);
	mat_mult(y_Rot, z_Rot, f_Rot);

	}else{
		y_Rot[1][1] = cos(d2);
		y_Rot[1][2] = 0;
		y_Rot[1][3] = sin(d2);
		y_Rot[2][1] = 0;
		y_Rot[2][2] = 1;
		y_Rot[2][3] = 0;
		y_Rot[3][1] = -sin(d2);
		y_Rot[3][2] = 0;
		y_Rot[3][3] = cos(d2);

		mat_mult(x_Rot, y_Rot, f_Rot);

		target[0] = target[0]+t1;
		target[1] = target[1]+t2;


	}

	aux[0][0] = f_Rot[1][1];
	aux[0][1] = f_Rot[1][2];
	aux[0][2] = f_Rot[1][3];
	aux[1][0] = f_Rot[2][1];
	aux[1][1] = f_Rot[2][2];
	aux[1][2] = f_Rot[2][3];
	aux[2][0] = f_Rot[3][1];
	aux[2][1] = f_Rot[3][2];
	aux[2][2] = f_Rot[3][3];

	rotMat2Quat(aux[0], aux[1], aux[2], target_orientation);
	quatMultiplication(target_orientation, temp_target_orientation, target_orientation);

	target[3] = target_orientation[0];
	target[4] = target_orientation[1];
	target[5] = target_orientation[2];
	target[6] = target_orientation[3];

}

int force_feedback_control(Vector X_dot, int* firsttimeptr)
{
	int i, r, c;
	double force_error;

#ifdef DEBUG_FORCES
	printf("\nraw force in sensor space: %lf %lf %lf\n", misc_sensor[1], misc_sensor[2], misc_sensor[3]);
	printf("base force in sensor space: %lf %lf %lf\n", FTSenseBase[1], FTSenseBase[2], FTSenseBase[3]);
	printf("zeored force in sensor space: %lf %lf %lf\n", f_s[1], f_s[2], f_s[3]);
	printf("force in world frame: %lf %lf %lf\n\n", f_world[1], f_world[2],f_world[3]);
	printf("raw torque in sensor space: %lf %lf %lf\n", misc_sensor[4], misc_sensor[5], misc_sensor[6]);
	printf("base torque in sensor space: %lf %lf %lf\n", FTSenseBase[4], FTSenseBase[5], FTSenseBase[6]);
	printf("zeroed torque in sensor space: %lf %lf %lf\n", f_s[4], f_s[5], f_s[6]);
	printf("torque in world frame: %lf %lf %lf\n\n", f_world[4], f_world[5],f_world[6]);
	fflush(stdout);
#endif // DEBUG_FORCES
	// Force feedback control gains
	// desired force is in opposite direction to measured force
	// deltaX[k] = SigmaK*(f_d - f_s[k]) + SigmaD*(f_s[k] - f_s[k-1])
	vec_sub(f_desired, f_world, f_diff_P);
	vec_sub(f_world_k_minus_1, f_world, f_diff_D);
	mat_vec_mult(Sigma_P, f_diff_P, X_dot_P);
	mat_vec_mult(Sigma_D, f_diff_D, X_dot_D);
	vec_add(X_dot_P, X_dot_D, X_dot);

#ifdef DEBUG_FORCES
	printf("X_dot_P = [");
	for (i = 1; i <= 6; ++i)
	{
		printf("%lf ", X_dot_P[i]);
	}
	printf("]\n");
	printf("X_dot_D = [");
	for (i = 1; i <= 6; ++i)
	{
		printf("%lf ", X_dot_D[i]);
	}
	printf("]\n");
	fflush(stdout);
#endif // DEBUG_FORCES

	// Copy current rotated forces to use for next time step
	for (i = 1; i <= 6; i++)
	{
		f_world_k_minus_1[i] = f_world[i];
	}

#ifdef DEBUG_FORCES
	printf("delta x: %lf %lf %lf\n", X_dot[1], X_dot[2], X_dot[3]);
	printf("delta w: %lf %lf %lf\n\n", X_dot[4], X_dot[5], X_dot[6]);
	fflush(stdout);
#endif // DEBUG_FORCES
	vec_sum(f_diff_P, &force_error);
	force_error = force_error/3;
	return (use_at_desired_force && at_desired_force(force_error));
}

int hybrid_force_velocity_control(Vector v_desired, double force_desired, Vector contact_normal,
                                  Vector force_sensed_w, Vector X_dot)
{
	int i, r, c;
	double v_force, v_target, force_error, force_norm;
	for (i = 1; i <= 3; ++i)
	{
		force_sensed_w3[i] = force_sensed_w[i];
	}
	// Get desired force feedback
	force_norm = vec_mult_inner(contact_normal, force_sensed_w3);
	force_error = force_norm - force_desired;
	force_integral = force_integral + force_error;

	v_force = /*clip_magnitude(force_gain_I*force_integral, MAX_FORCE_INTEGRAL_MAGNITUDE) + */force_gain_P*force_error;
	// Project force vector onto contact normal
	vec_mult_scalar(contact_normal, v_force, u_force);

	// Project desired velocity onto orthogonal complement of normal P(n) = I_3 - n*n^t
	get_ortho_projection_mat(contact_normal, P_bar);
	mat_vec_mult(P_bar, v_desired, u_velocity);

	// Add velocities to give a control signal to the J_inv controller
	vec_sub(u_velocity, u_force, u_hybrid);
	for (i = 1; i <= 3; ++i)
	{
		X_dot[i] = u_hybrid[i];
	}
	for (i = 4; i <= 6; ++i)
	{
		X_dot[i] = 0.0;
	}
#ifdef DEBUG_HYBRID_FORCE_CONTROL
	// print_mat("P_bar", P_bar);
	// print_vec("n_c", contact_normal);
	printf("force_norm = %lf\n", force_norm);
	printf("force_error = %lf\n", force_error);
	// printf("force_integral = %lf\n", force_integral);
	printf("v_force = %lf\n", v_force);
	printf("u_force = [%lf, %lf, %lf]\n", u_force[1], u_force[2], u_force[3]);
	printf("u_velocity = [%lf, %lf, %lf]\n", u_velocity[1], u_velocity[2], u_velocity[3]);
	fflush(stdout);
#endif // DEBUG_HYBRID_FORCE_CONTROL

	return (use_at_desired_force && at_desired_force(force_error));
}

int hybrid_bt_pressure_velocity_control(Vector v_desired, double pressure_desired, Vector contact_normal, Vector X_dot)
{
	int i, c;
	double v_pressure, pressure_error, u_h_norm, r;

	pressure_error = pressure_desired - (misc_sensor[bt_PDC] - PDC_tare);

	v_pressure = pressure_gain_P*pressure_error + pressure_gain_D*(pressure_error-pressure_delta) + pressure_gain_I*(pressure_error+pressure_Integral);
	//printf("Pres_des: %f\tP: %f\tD: %f\tI: %f\n", pressure_desired, pressure_gain_P*pressure_error, pressure_gain_D*(pressure_error-pressure_delta), pressure_gain_I*(pressure_error+pressure_Integral));

	if(misc_sensor[bt_PDC] - PDC_tare > 300){
		v_pressure = -0.05;
	}
	//printf("PDC: %f\n", misc_sensor[bt_PDC] - PDC_tare);
	//print_vec("normal", contact_normal);
	vec_mult_scalar(contact_normal, v_pressure, u_force);
	//print_vec("u_force", u_force);
	u_h_norm = sqrt(pow(u_force[1],2.0) + pow(u_force[2],2.0) + pow(u_force[3],2.0));

	//printf("norm: %f\t%f\t%f\t%f\n", pow(u_hybrid[1],2), pow(u_hybrid[1],2), pow(u_hybrid[1],2), u_h_norm);

	if (u_h_norm > 0.05){
		r = 1 - ((u_h_norm - 0.05)/u_h_norm);
		vec_mult_scalar(u_force, r, u_force);
	}

	vec_add(v_desired, u_force, u_hybrid);



	for (i = 1; i <= 3; ++i)
	{
		X_dot[i] = u_hybrid[i];
	}
	for (i = 4; i <= 6; ++i)
	{
		X_dot[i] = 0.0;
	}
	pressure_delta = pressure_error;

	return (use_at_desired_force && at_desired_force(pressure_error));
}


void update_contact_loc_estimate(Vector r_hat_e, Matrix L_r, Vector c_r, Vector p_hat_c,
                                 Vector force_sensed)
{
	int i;
	// Time derivatives of the current estimates
	Vector r_hat_e_dot;
	Matrix L_r_dot;
	Vector c_r_dot;
	Vector delta_r;
	Matrix delta_L;
	Vector delta_c;
	// Sensed forces broken into seperate vectors
	Vector f_e;
	Vector tau_e;
	// Other meaningful state variables
	Vector p_e;
	Vector r_hat;
	// Intermediate math variables
	Vector Lr_product;
	Vector Lr_diff_cr;
	Matrix S_force;
	Matrix SS_force;
	Matrix beta_L_r;
	Vector S_f_tau;
	Vector beta_c_r;
	r_hat_e_dot = my_vector(1,3);
	L_r_dot = my_matrix(1,3,1,3);
	c_r_dot = my_vector(1,3);
	delta_r = my_vector(1,3);
	delta_L = my_matrix(1,3,1,3);
	delta_c = my_vector(1,3);
	f_e = my_vector(1,3);
	tau_e = my_vector(1,3);
	p_e = my_vector(1,3);
	r_hat = my_vector(1,3);
	Lr_product = my_vector(1,3);
	Lr_diff_cr = my_vector(1,3);
	S_force = my_matrix(1,3,1,3);
	SS_force = my_matrix(1,3,1,3);
	S_f_tau = my_vector(1,3);
	beta_c_r = my_vector(1,3);
	beta_L_r = my_matrix(1,3,1,3);
	// Split forces into two separate vector in R^3 for the forces and torques
	for (i = 1; i <= 3; ++i)
	{
		f_e[i] = force_sensed[i];
	}
	for (i = 1; i <= 3; ++i)
	{
		tau_e[i] = force_sensed[i+3];
	}

	// Compute the updates for the state variables
	// r_hat_e_dot = Gamma_r*[L_r*r_hat_e - c_r]
	mat_vec_mult(L_r, r_hat_e, Lr_product);
	vec_sub(Lr_product, c_r, Lr_diff_cr);
	mat_vec_mult(Gamma_r, Lr_diff_cr, r_hat_e_dot);
	// L_r_dot = -beta_r*L_r - S(f_e)*S(f_e)
	make_cross_product_matrix(f_e, S_force);
	mat_mult(S_force, S_force, SS_force);
	mat_mult_scalar(L_r, -beta_r, beta_L_r);
	mat_sub(beta_L_r, SS_force, L_r_dot);
	// c_r_dot = -beta_r*c_r + S(f_e)*tau_e
	mat_vec_mult(S_force, tau_e, S_f_tau);
	vec_mult_scalar(c_r, -beta_r, beta_c_r);
	vec_add(beta_c_r, S_f_tau, c_r_dot);

	// Estiamte the current contact vector using discrete time updates
	vec_mult_scalar(r_hat_e_dot, time_step, delta_r);
	mat_mult_scalar(L_r_dot, time_step, delta_L);
	vec_mult_scalar(c_r_dot, time_step, delta_c);
	vec_add(r_hat_e, delta_r, r_hat_e);
	mat_add(L_r, delta_L, L_r);
	vec_add(c_r, delta_c, c_r);
#ifdef DEBUG_CONTACT_ESTIMATION
	printf("r_hat_e_dot = [%lf, %lf, %lf]\n", r_hat_e_dot[1], r_hat_e_dot[2], r_hat_e_dot[3]);
	printf("delta_r = [%lf, %lf, %lf]\n", delta_r[1], delta_r[2], delta_r[3]);
	printf("r_hat_e = [%lf, %lf, %lf]\n", r_hat_e[1], r_hat_e[2], r_hat_e[3]);
	// print_vec("f_sensed", force_sensed);
	// print_vec("f_e", f_e);
	// print_vec("tau_e", tau_e);
	// print_mat("S_force", S_force);
	// print_mat("SS_force", SS_force);
	// print_mat("delta_L", delta_L);
	// print_mat("L_r", L_r);
	// printf("delta_c = [%lf, %lf, %lf]\n", delta_c[1], delta_c[2], delta_c[3]);
	// printf("c_r = [%lf, %lf, %lf]\n", c_r[1], c_r[2], c_r[3]);
	fflush(stdout);
#endif // DEBUG_CONTACT_ESTIMATION
	// Estiamte the new contact location in world frame
	for (i = 1; i <= 3; ++i)
	{
		p_e[i] = cart_state[1].x[i];
	}
	// p_hat_c = p_e + R_sw*r_hat_e
	quatToRotMat(&cart_orient[1], R_ws);
	mat_vec_mult(R_sw, r_hat_e, r_hat);
	vec_add(p_e, r_hat, p_hat_c);
#ifdef DEBUG_CONTACT_ESTIMATION
	// print_mat("R_sw", R_sw);
	printf("P_hat_c = [%lf, %lf, %lf]\n", p_hat_c[1], p_hat_c[2], p_hat_c[3]);
	fflush(stdout);
#endif // DEBUG_CONTACT_ESTIMATION
	// Cleanup before leaving
	my_free_vector(r_hat_e_dot, 1,3);
	my_free_matrix(L_r_dot, 1,3,1,3);
	my_free_vector(c_r_dot, 1,3);
	my_free_vector(delta_r,1,3);
	my_free_matrix(delta_L, 1,3,1,3);
	my_free_vector(delta_c, 1,3);
	my_free_vector(f_e, 1,3);
	my_free_vector(tau_e, 1,3);
	my_free_vector(p_e, 1, 3);
	my_free_vector(r_hat, 1, 3);
	my_free_vector(Lr_product, 1,3);
	my_free_vector(Lr_diff_cr, 1,3);
	my_free_matrix(S_force, 1,3,1,3);
	my_free_matrix(SS_force, 1,3,1,3);
	my_free_vector(S_f_tau, 1,3);
	my_free_vector(beta_c_r, 1,3);
	my_free_matrix(beta_L_r, 1,3,1,3);
}

void update_contact_normal_estimate(Vector n_hat_c, Matrix L_n)
{
	int i;
	// State variable time derivatives
	Vector n_hat_c_dot;
	Matrix L_n_dot;
	Matrix p_dot;
	Matrix p_e_dot;
	Vector delta_n;
	Matrix delta_L;
	// Intermeidiate math variables
	Vector L_n_prod;
	Vector P_L_n_prod;
	Matrix P_bar_n_c;
	Matrix beta_L_n;
	Matrix p_e_dot_outer;
	Matrix p_e_dot_prods;
	float p_normalize;
	float p_dot_squared_norm = 0.0;
	n_hat_c_dot = my_vector(1,3);
	L_n_dot = my_matrix(1,3,1,3);
	p_dot = my_matrix(1,3, 1, 1);
	p_e_dot = my_matrix(1,3, 1, 1);
	delta_n = my_vector(1,3);
	delta_L = my_matrix(1,3,1,3);
	L_n_prod = my_vector(1,3);
	P_L_n_prod = my_vector(1,3);
	P_bar_n_c = my_matrix(1,3,1,3);
	beta_L_n = my_matrix(1,3,1,3);
	p_e_dot_outer = my_matrix(1,3,1,3);
	p_e_dot_prods = my_matrix(1,3,1,3);

	// Get current end effector velocities
	// Need to rotate these into the EE frame
	for (i = 1; i <= 3; ++i)
	{
		p_dot[i][1] = cart_state[1].xd[i];
	}
	quatToRotMat(&cart_orient[1], R_ws);
	mat_mult(R_ws, p_dot, p_e_dot);

	// n_hat_c_dot = -gamma_n*P_bar(n_c)*L_n*n_hat_c
	mat_vec_mult(L_n, n_hat_c, L_n_prod);
	get_ortho_projection_mat(n_hat_c, P_bar_n_c);
	mat_vec_mult(P_bar_n_c, L_n_prod, P_L_n_prod);
	vec_mult_scalar(P_L_n_prod, -gamma_n, n_hat_c_dot);
	// L_n_dot = -beta_n*L_n + 1/(1+(p1*2+p2*2+p3*2))*p_e_dot*p_e_dot^T
	mat_mult_scalar(L_n, -beta_n, beta_L_n);
	mat_mult_normal_transpose(p_e_dot, p_e_dot, p_e_dot_outer);

	for (i = 1; i <= 3; ++i)
	{
		p_dot_squared_norm += p_e_dot[i][1]*p_e_dot[i][1];
	}

	p_normalize = 1.0/(1.0+p_dot_squared_norm);
	mat_mult_scalar(p_e_dot_outer, p_normalize, p_e_dot_prods);
	mat_add(beta_L_n, p_e_dot_prods, L_n_dot);

	// Update estimates of the current state variables
	vec_mult_scalar(n_hat_c_dot, time_step, delta_n);
	mat_mult_scalar(L_n_dot, time_step, delta_L);
	vec_add(n_hat_c, delta_n, n_hat_c);
	mat_add(L_n, delta_L, L_n);

#ifdef DEBUG_CONTACT_ESTIMATION
	print_mat("P_e_dot", p_e_dot);
	print_mat("P_bar_n_c", P_bar_n_c);
	printf("n_hat_c = [%lf, %lf, %lf]\n", n_hat_c[1], n_hat_c[2], n_hat_c[3]);
	print_mat("L_n", L_n);
	fflush(stdout);
#endif // DEBUG_CONTACT_ESTIMATION

	// Cleanup
	my_free_vector(n_hat_c_dot, 1, 3);
	my_free_matrix(L_n_dot, 1,3,1,3);
	my_free_matrix(p_e_dot, 1,3,1,1);
	my_free_matrix(p_dot, 1,3,1,1);
	my_free_vector(delta_n,1,3);
	my_free_matrix(delta_L, 1,3,1,3);
	my_free_vector(L_n_prod, 1,3);
	my_free_vector(P_L_n_prod, 1,3);
	my_free_matrix(P_bar_n_c, 1,3,1,3);
	my_free_matrix(beta_L_n, 1,3,1,3);
	my_free_matrix(p_e_dot_outer, 1,3,1,3);
	my_free_matrix(p_e_dot_prods, 1,3,1,3);
}

int move_until_contact_control(Vector v_desired, double force_threshold, Vector force_sensed,
                               Vector X_dot)
{
	int i;
	int should_stop;
	double total_force_applied = 0.0;

	// Check if forces sensed are above the stop thereshold
	for (i = 1; i <= 3; ++i)
	{
		total_force_applied += fabs(force_sensed[i]);
#ifdef DEBUG_CONTACT_THRESH_CONTROL
		// printf("Sensor[%i] = %lf\n", i, force_sensed[i]);
		// printf("Sensor baseline[%i] = %lf\n", i, FTSenseBase[i]);
		// printf("force[%i] = %lf\n", i, force_sensed[i]);
		printf("total_force_applied[%i] = %lf\n", i, total_force_applied);
#endif // DEBUG_CONTACT_THRESH_CONTROL
	}
#ifdef DEBUG_CONTACT_THRESH_CONTROL
	printf("total_force_applied = %lf\n", total_force_applied);
	fflush(stdout);
#endif // DEBUG_CONTACT_THRESH_CONTROL

	if (total_force_applied > force_threshold)
	{
		desired_force_count_ += 1;
#ifdef DEBUG_CONTACT_THRESH_CONTROL
		printf("Incrementing desired force count\n");
		printf("Desired force count = %i\n\n", desired_force_count_);
		fflush(stdout);
#endif // DEBUG_CONTACT_THRESH_CONTROL
		// TODO: Change motion to be negative of applied force with magnitude scaled accordingly
		for (i = 1; i <= 3; ++i)
		{
			X_dot[i] = v_desired[i];
		}
		for (i = 4; i <= 6; ++i)
		{
			X_dot[i] = 0.0;
		}
	}
	else
	{
#ifdef DEBUG_CONTACT_THRESH_CONTROL
		if(desired_force_count_ > 1)
		{
			printf("Resetting desired force count\n\n");
			fflush(stdout);
		}
		else
		{
			printf("\n");
			fflush(stdout);
		}
#endif // DEBUG_CONTACT_THRESH_CONTROL
		desired_force_count_ = 0;
		// Set desired motion to be the current command velocity
		for (i = 1; i <= 3; ++i)
		{
			X_dot[i] = v_desired[i];
		}
		for (i = 4; i <= 6; ++i)
		{
			X_dot[i] = 0.0;
		}
	}

	should_stop = (desired_force_count_ >= DESIRED_FORCE_COUNT_THRESH);
	if (should_stop)
	{
		printf("Reached desired contact force of %lf\t[%lf, %lf %lf]\n",
	   		total_force_applied, force_sensed[1], force_sensed[2], force_sensed[31]);
		fflush(stdout);
	}

	return should_stop;
}

int move_until_contact_biotac_control(Vector v_desired, double PDC_threshold, Vector X_dot)
{
	int i;
	int should_stop;
	double PDC_diff = misc_sensor[bt_PDC] - PDC_tare;

	// Check if forces sensed are above the stop thereshold
#ifdef DEBUG_CONTACT_THRESH_CONTROL
	printf("Checking biotac thresh for diff of %lf from tare of %lf\t", PDC_diff, PDC_tare);
	printf("PDC_trheshold: %lf\n", PDC_threshold);
	fflush(stdout);
#endif // DEBUG_CONTACT_THRESH_CONTROL

	if (PDC_diff > PDC_threshold)
	{
		// TODO: Change motion to be negative of applied force with magnitude scaled accordingly
		for (i = 1; i <= 6; ++i)
		{
			X_dot[i] = 0.0;
		}
		should_stop = TRUE;
	}
	else
	{
		desired_force_count_ = 0;
		// Set desired motion to be the current command velocity
		for (i = 1; i <= 3; ++i)
		{
			X_dot[i] = v_desired[i];
		}
		for (i = 4; i <= 6; ++i)
		{
			X_dot[i] = 0.0;
		}
		should_stop = FALSE;
	}
	/*if (should_stop)
	{
		printf("Reached desired contact PDC threshold of %lf: %lf\n", PDC_threshold, PDC_diff);
		fflush(stdout);
	}*/
	return should_stop;
}

int move_task_velocity_control(Vector v_desired, Vector X_dot)
{
	int i;
	for (i = 1; i <= 3; ++i)
	{
		X_dot[i] = v_desired[i];
	}
	for (i = 4; i <= 6; ++i)
	{
		X_dot[i] = 0.0;
	}
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////
// Helper functions
/////////////////////////////////////////////////////////////////////////////////////

int time_is_up()
{
	return time_is_up_base(FALSE);
}

int time_is_up_base(int print_time)
{
	double current_force_move_time = getTimeFloat();
	double elapsed_time = current_force_move_time - force_move_start_time;
	if (print_time)
	{
		printf("Elapsed time: %lf\n", elapsed_time);
		fflush(stdout);
	}
	return elapsed_time >= des_force_move_time;
}

/**
 * Function to transform a wrench in sensor frame to equivalent wrench in the robot base frame
 *
 * @param f_sensor [in]  wrench in sensor space
 * @param f_world  [out] wrench in world space
 */
void ft_sensor_wrench_in_world_frame(Vector f_sensor, Vector f_w)
{
	quatToRotMat(&cart_orient[1], R_ws);
	mat_trans(R_ws, R_sw);
	// Get P_sw (world frame coordinates in sensor frame) (i.e. -hand coordinates)
	P_sw[_X_] = -cart_state[1].x[_X_];
	P_sw[_Y_] = -cart_state[1].x[_Y_];
	P_sw[_Z_] = -cart_state[1].x[_Z_];

	wrench_A_in_B_frame(f_sensor, R_sw, P_sw, f_w);
}

/**
 * Function to transform a wrench in world frame to the equivalent wrench in the sensor frame
 *
 * @param f_world  [in]  wrench in world frame
 * @param f_sensor [out] wrench in sensor frame
 */
void world_wrench_in_ft_sensor_frame(Vector f_w, Vector f_sensor)
{
	quatToRotMat(&cart_orient[1], R_ws);
	// Get P_sw (sensor frame coordinates in world frame) (i.e. hand coordinates)
	P_ws[_X_] = cart_state[1].x[_X_];
	P_ws[_Y_] = cart_state[1].x[_Y_];
	P_ws[_Z_] = cart_state[1].x[_Z_];

	wrench_A_in_B_frame(f_w, R_ws, P_ws, f_sensor);
}

/**
 * Transform a wrench represented in frame A to equivalent wrench in frame B
 *
 * @param f_A  [in]  wrench in frame A
 * @param R_AB [in]  rotation matrix of B relative to A
 * @param P_AB [in]  position vector of B relative to A
 * @param f_B  [out] wrench in frame B equivalent to f_A
 */
void wrench_A_in_B_frame(Vector f_A, Matrix R_AB, Vector P_AB, Vector f_B)
{
	int r, c;
	// Get matrix transpose
	mat_trans(R_AB, R_AB_trans);

	// Get the cross product matrix for -P_AB
	make_cross_product_matrix_neg(P_AB, P_AB_hat_neg);

	// Compute lower left to R^t X PHat_AB
	mat_mult(R_AB_trans, P_AB_hat_neg, transform_ll);

	for (r = 1; r <= 3; ++r)
	{
		for (c = 1; c <= 3; ++c)
		{
			// Set upper left to R^T
			W_transform_AB[r][c] = R_AB_trans[r][c];
			// Set lower right to R^T
			W_transform_AB[r+3][c+3] = R_AB_trans[r][c];
		}
	}
	// Set upper right to 0
	for (r = 1; r <= 3; ++r)
	{
		for (c = 4; c <= 6; ++c)
		{
			W_transform_AB[r][c] = 0.0;
		}
	}
	// Set lower left to R^t X PHat_AB
	for (r = 1; r <= 3; ++r)
	{
		for (c = 1; c <= 3; ++c)
		{
			W_transform_AB[r+3][c] = transform_ll[r][c];
		}
	}
	mat_vec_mult(W_transform_AB, f_A, f_B);
}

int at_desired_force(double force_error)
{
	// printf("force_error = %lf\n", force_error);
	// printf("|force_error| = %lf\n", fabs(force_error));
	// fflush(stdout);

	if ( fabs(force_error) < DESIRED_FORCE_EPSILON)
	{
		desired_force_count_ += 1;
		// printf("Below force threshold");
		// printf("desired_force_count_ = %i\n", desired_force_count_);
		// fflush(stdout);
	}
	else
	{
		desired_force_count_ = 0;
	}
	return (desired_force_count_ >= DESIRED_FORCE_COUNT_THRESH);
}

void make_cross_product_matrix(Vector x, Matrix S)
{
	make_cross_product_matrix_base(x, S, FALSE);
}

void make_cross_product_matrix_neg(Vector x, Matrix S)
{
	make_cross_product_matrix_base(x, S, TRUE);
}

void make_cross_product_matrix_base(Vector x, Matrix S, int is_neg)
{
	float a = 1;
	if (is_neg)
	{
		a = -1;
	}
	// Get the cross product matrix for -P_AB
	S[1][1] =  0.0;
	S[1][2] = -a*x[_Z_];
	S[1][3] =  a*x[_Y_];
	S[2][1] =  a*x[_Z_];
	S[2][2] =  0.0;
	S[2][3] = -a*x[_X_];
	S[3][1] = -a*x[_Y_];
	S[3][2] =  a*x[_X_];
	S[3][3] =  0.0;
}

void get_ortho_projection_mat(Vector a, Matrix P_bar_local)
{
	int i;
	for (i = 1; i <= 3; ++i)
	{
		a_mat[i][1] = a[i];
	}
	mat_mult_normal_transpose(a_mat, a_mat, a_prod);

	mat_sub(I3, a_prod, P_bar_local);
}

int biotac_working()
{
	// TODO: Check if other parts are working...
	return (misc_sensor[bt_PDC] != 0.0);
}
