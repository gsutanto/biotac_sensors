#Instructions:
## Add udev rules:
Support for udev requires a single configuration file that is available on the software CD, and also listed on the Total Phase website for download. This file is 99-totalphase.rules. Please follow the following steps to enable the appropriate permissions for the Cheetah adapter.

    As superuser, copy system_files/99-totalphase.rules to /etc/udev/rules.d
    chmod 644 /etc/udev/rules.d/99-totalphase.rules
    Unplug and replug your Cheetah adapter(s)

## Running code:
  - Download  and compile this repo into your workspace.
  - run: rosrun biotac_sensors biotac_pub