import rospy
from biotac_sensors.msg import *
import numpy as np
from geometry_msgs.msg import WrenchStamped

class biotacAPI:
    def __init__(self,init_node=False,loop_rate=100.0,fingers=[0]):
        print "Initializing biotac sensors"
        if(init_node):
            rospy.init_node("biotac_api_node")
        
        self.loop_rate=rospy.Rate(loop_rate)
        # Subscribe to biotac data:
        self.got_biotac_data=False
        rospy.Subscriber('/biotac_pub',BioTacHand,self.biotac_cb)

        self.got_biotac_force=False
        # Subscribe to contact sensing:
        if 0 in fingers:
            rospy.Subscriber('/biotacs/index/contact_position_normal',WrenchStamped,self.ind_contact_cb)

        self.contact_position=[np.zeros(3) for i in range(len(fingers))]
        self.contact_force=[np.zeros(3) for i in range(len(fingers))]
        

    def get_force_data(self):
        while(not self.got_biotac_force):
            self.loop_rate.sleep()

    def biotac_cb(self,msg):
        if(not self.got_biotac_data):
            self.num_sensors=len(msg.bt_data)
            print "Found: "+str(self.num_sensors)+" Biotac sensors"
            self.bio_data=[[] for i in range(self.num_sensors)]
            self.tare=[[] for i in range(self.num_sensors)]
            self.got_biotac_data=True
        for i in range(self.num_sensors):
            self.bio_data[i]=msg.bt_data[i]

    def update_contact_data(self,msg,i):
        self.contact_position[i]=np.ravel([msg.wrench.force.x,msg.wrench.force.y,msg.wrench.force.z])
        self.contact_force[i]=np.ravel([msg.wrench.torque.x,msg.wrench.torque.y,msg.wrench.torque.z])
        self.got_biotac_force=True
        
    def ind_contact_cb(self,msg):
        self.update_contact_data(msg,0)

'''
if __name__=='__main__':
    bt=biotacAPI(True)
    bt.get_force_data()
    
    print bt.contact_position[0]
    print bt.contact_force[0]

'''
