#!/usr/bin/env python
# Computes the contact positions and normals on the biotac sensors
import rospy
from biotac_sensors.msg import *
import numpy as np
from geometry_msgs.msg import WrenchStamped
import tf

#from stl import mesh
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import norm
from rospkg import RosPack
import copy
class biotacSensing:
    def __init__(self,node_name='biotac_contact_node',loop_rate=100):
        rospy.init_node(node_name)
        self.rate=rospy.Rate(loop_rate)
        # Subscribe to biotac data:
        rospy.Subscriber('/biotac_pub',BioTacHand,self.biotac_cb)
        # Create publisher to publish contact position and force
        self.contact_pose=rospy.Publisher('biotacs/index/contact_position_normal',WrenchStamped,queue_size=1)
        self.got_biotac_data=False
        # initializing sensor parameters:
        self.num_electrodes=19
        self.got_initial_offsets=False
        self.elect_positions=np.zeros((19,3))
        self.update_elect_positions()
        self.pres_thresh_gain=1.005
        self.IN_CONTACT=False
    def update_elect_positions(self):
        elect_position=np.zeros((20,4))
        elect_position[1][1] = 0.993;
	elect_position[1][2] = -4.855;
	elect_position[1][3] = -1.116;
	elect_position[2][1] = -2.700;
	elect_position[2][2] = -3.513;
	elect_position[2][3] = -3.670;
	elect_position[3][1] = -6.200;
	elect_position[3][2] = -3.513;
	elect_position[3][3] = -3.670;
	elect_position[4][1] = -8.000;
	elect_position[4][2] = -4.956;
	elect_position[4][3] = -1.116;
	elect_position[5][1] = -10.500;
	elect_position[5][2] = -3.513;
	elect_position[5][3] = -3.670;
	elect_position[6][1] = -13.400;
	elect_position[6][2] = -4.956;
	elect_position[6][3] = -1.116;
	elect_position[7][1] = 4.763;
	elect_position[7][2] = 0.000;
	elect_position[7][3] = -2.330;
	elect_position[8][1] = 3.031;
	elect_position[8][2] = -1.950;
	elect_position[8][3] = -3.330;
	elect_position[9][1] = 3.031;
	elect_position[9][2] = 1.950;
	elect_position[9][3] = -3.330;
	elect_position[10][1] = 1.299;
	elect_position[10][2] = 0.000;
	elect_position[10][3] = -4.330;
	elect_position[11][1] = 0.993;
	elect_position[11][2] = 4.855;
	elect_position[11][3] = -1.116;
	elect_position[12][1] = -2.700;
	elect_position[12][2] = 3.513;
	elect_position[12][3] = -3.670;
	elect_position[13][1] = -6.200;
	elect_position[13][2] = 3.513;
	elect_position[13][3] = -3.670;
	elect_position[14][1] = -8.000;
	elect_position[14][2] = 4.956;
	elect_position[14][3] = -1.116;
	elect_position[15][1] = -10.500;
	elect_position[15][2] = 3.513;
	elect_position[15][3] = -3.670;
	elect_position[16][1] = -13.400;
	elect_position[16][2] = 4.956;
	elect_position[16][3] = -1.116;
	elect_position[17][1] = -2.800;
	elect_position[17][2] = 0.000;
	elect_position[17][3] = -5.080;
	elect_position[18][1] = -9.800;
	elect_position[18][2] = 0.000;
	elect_position[18][3] = -5.080;
	elect_position[19][1] = -13.600;
	elect_position[19][2] = 0.000;
	elect_position[19][3] = -5.080;
        self.elect_positions=elect_position[1:20,1:4]

        # Normals:
        elect_normal=np.zeros((20,4))
        elect_normal[1][1] = 0.196;
	elect_normal[1][2] = -0.956;
	elect_normal[1][3] = 0.220;
	elect_normal[2][1] = 0.0;
	elect_normal[2][2] = -0.692;
	elect_normal[2][3] = -0.722;
	elect_normal[3][1] = 0.0;
	elect_normal[3][2] = -0.692;
	elect_normal[3][3] = -0.722;
	elect_normal[4][1] = 0.0;
	elect_normal[4][2] = -0.976;
	elect_normal[4][3] = -0.220;
	elect_normal[5][1] = 0.0;
	elect_normal[5][2] = -0.976;
	elect_normal[5][3] = -0.220;
	elect_normal[6][1] = 0.5;
	elect_normal[6][2] = 0.0;
	elect_normal[6][3] = -0.866;
	elect_normal[7][1] = 0.5;
	elect_normal[7][2] = 0.0;
	elect_normal[7][3] = -0.866;
	elect_normal[8][1] = 0.5;
	elect_normal[8][2] = 0.0;
	elect_normal[8][3] = -0.866;
	elect_normal[9][1] = 0.5;
	elect_normal[9][2] = 0.0;
	elect_normal[9][3] = -0.866;
	elect_normal[10][1] = 0.5;
	elect_normal[10][2] = 0.0;
	elect_normal[10][3] = -0.866;
	elect_normal[11][1] = 0.196;
	elect_normal[11][2] = 0.956;
	elect_normal[11][3] = -0.220;
	elect_normal[12][1] = 0.0;
	elect_normal[12][2] = 0.692;
	elect_normal[12][3] = -0.722;
	elect_normal[13][1] = 0.0;
	elect_normal[13][2] = 0.692;
	elect_normal[13][3] = -0.722;
	elect_normal[14][1] = 0.0;
	elect_normal[14][2] = 0.976;
	elect_normal[14][3] = -0.220;
	elect_normal[15][1] = 0.0;
	elect_normal[15][2] = 0.692;
	elect_normal[15][3] = -0.722;
	elect_normal[16][1] = 0.0;
	elect_normal[16][2] = 0.976;
	elect_normal[16][3] = -0.220;
	elect_normal[17][1] = 0.0;
	elect_normal[17][2] = 0.0;
	elect_normal[17][3] = -1.000;
	elect_normal[18][1] = 0.0;
	elect_normal[18][2] = 0.0;
	elect_normal[18][3] = -1.000;
	elect_normal[19][1] = 0.0;
	elect_normal[19][2] = 0.0;
	elect_normal[19][3] = -1.000;
        self.elect_normal=elect_normal[1:20,1:4]
        
    def biotac_cb(self,msg):
        if(not self.got_biotac_data):
            self.num_sensors=len(msg.bt_data)
            self.bio_data=[[] for i in range(self.num_sensors)]
            self.tare=[[] for i in range(self.num_sensors)]
            self.got_biotac_data=True
        if(self.got_initial_offsets):
            if( msg.bt_data[0].pdc_data>=self.tare_pdc):
                self.IN_CONTACT=True
            else:
                self.IN_CONTACT=False
        for i in range(self.num_sensors):
            self.bio_data[i]=msg.bt_data[i]

    def get_initial_offsets(self,idx):
        self.tare[0]=np.array(self.bio_data[idx].electrode_data)
        self.tare_pdc=copy.deepcopy(self.pres_thresh_gain*self.bio_data[0].pdc_data)
        print 'Stored initial offsets for electrodes'
        self.got_initial_offsets=True

    def compute_contact_position(self,idx,R):
        # Computes the contact position of idx sensor:
        #weights=np.zeros(self.num_electrodes)
        electrode_data=self.electrode_data_norm[idx]**2 # squared as per paper
        normalized_data=electrode_data/np.sum(electrode_data)
        contact_pt=np.matrix(normalized_data)*self.elect_positions #[1x19]* [19*3]= [1x3] centroid
        contact_pt=np.ravel(contact_pt)
        v_norm=0.0
        r=R
        if(contact_pt[0]>0.0):
            for j in range(3):
                v_norm+=contact_pt[j]**2
            contact_pt=contact_pt*(r/np.sqrt(v_norm))
        else:
            j=1
            while(j<3):
                v_norm+=contact_pt[j]**2
                j+=1
            j=1
            while(j<3):
                contact_pt[j]=contact_pt[j]*(r/np.sqrt(v_norm))
                j+=1
                
        #print contact_pt
        return np.ravel(contact_pt)#/1000.0

    def compute_contact_normal(self,idx):
        electrode_data=self.electrode_data_norm[idx]**2
        normal=np.matrix(electrode_data)*self.elect_normal
        normal_vec=np.ravel(normal)/np.linalg.norm(normal)
        #print normal
        return np.ravel(normal_vec)#/1000.0
    def compute_norm_data(self):
        self.electrode_data_norm=[]
        for i in range(self.num_sensors):
            self.electrode_data_norm.append(np.array(self.bio_data[i].electrode_data-self.tare[i],dtype=np.float32))
    def pub_wrench(self,position,normal):
        new_pose=WrenchStamped()
    
        new_pose.header.frame_id='biotac_frame'
        new_pose.header.stamp=rospy.Time.now()
        new_pose.wrench.force.x=position[0]
        new_pose.wrench.force.y=position[1]
        new_pose.wrench.force.z=position[2]
        new_pose.wrench.torque.x=normal[0]
        new_pose.wrench.torque.y=normal[1]
        new_pose.wrench.torque.z=normal[2]
        self.contact_pose.publish(new_pose)

def plot_biotac_shape(p0,p1,R,ax):
    
    #vector in direction of axis
    v = p1 - p0

    #find magnitude of vector
    mag = norm(v)

    #unit vector in direction of axis
    v = v / mag

    #make some vector not in the same direction as v
    not_v = np.array([1, 0, 0])
    if (v == not_v).all():
        not_v = np.array([0, 1, 0])

    #make vector perpendicular to v
    n1 = np.cross(v, not_v)
    #normalize n1
    n1 /= norm(n1)

    #make unit vector perpendicular to v and n1
    n2 = np.cross(v, n1)

    #surface ranges over t from 0 to length of axis and 0 to 2*pi
    t = np.linspace(0, mag, 2)
    theta = np.linspace(np.pi, 2 * np.pi, 100)
    rsample = np.linspace(0, R, 2)

    #use meshgrid to make 2d arrays
    t, theta2 = np.meshgrid(t, theta)

    rsample,theta = np.meshgrid(rsample, theta)
    
    #generate coordinates for surface
    # "Tube"
    X, Y, Z = [p0[i] + v[i] * t + R * np.sin(theta2) * n1[i] + R * np.cos(theta2) *       n2[i] for i in [0, 1, 2]]

    ax.plot_surface(X, Y, Z, color='green',alpha=1.0)
    #draw sphere
    u = np.linspace(0.5*np.pi, np.pi, 30)
    v = np.linspace(0, 1* np.pi, 30)

    x = R*np.outer(np.sin(u), np.sin(v))
    y = R*np.outer(np.sin(u), np.cos(v))
    z = R*np.outer(np.cos(u), np.ones_like(v))

    ax.plot_surface(x, y, z, color="green",alpha=1.0)
    return ax

def main():
    fig=plt.figure()
    biotac=biotacSensing()
    plt.ion()

    ax = fig.add_subplot(111,projection='3d')
    i=0
    rad=5.5
    position=np.zeros(3)
    normal=np.zeros(3)
    while not rospy.is_shutdown():
        if biotac.got_biotac_data:
            if not biotac.got_initial_offsets:
                biotac.get_initial_offsets(0)
            else:
                ax.cla()
                biotac.compute_norm_data()
                if(biotac.IN_CONTACT):
                    position=biotac.compute_contact_position(0,rad)
                    normal=biotac.compute_contact_normal(0)-position
                else:
                    position=np.zeros(3)
                    normal=np.zeros(3)
                X=biotac.elect_positions[:,0]#*0.008#1000.0
                Y=biotac.elect_positions[:,1]#*0.008#1000.0
                Z=biotac.elect_positions[:,2]#*0.008#/1000.0


                
                p1=np.array([-13.4,0,0])
                p2=np.array([0,0,0])

                biotac.pub_wrench(position,normal)
                
                #Debug stuff:
                ax=plot_biotac_shape(p1,p2,rad,ax)
                
                # Plot a half-sphere:

                # Find closest point on the surface:

                ax.scatter([position[0]],[position[1]],[position[2]],color='red',s=np.pi*4.0)
                ax.quiver(position[0],position[1],position[2],normal[0],normal[1],normal[2],color='red',length=1.0)
                ax.grid(b=False)
                ax.set_xlim3d(-10, 10)
                ax.set_ylim3d(-10,10)
                ax.set_zlim3d(0,-6)
                plt.axis('off')

                plt.pause(0.0001)

                #if(biotac.IN_CONTACT):
                    #plt.savefig('biotac'+str(i)+'.png', bbox_inches='tight')
                    #i+=1
        biotac.rate.sleep()

main()

