#!/usr/bin/env python
# Computes the contact positions and normals on the biotac sensors
import rospy
from biotac_sensors.msg import *
import numpy as np
from geometry_msgs.msg import WrenchStamped
import tf

#from stl import mesh
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import norm
from rospkg import RosPack
import copy
class biotacSensing:
    def __init__(self,node_name='biotac_contact_node',loop_rate=100):
        rospy.init_node(node_name)
        self.rate=rospy.Rate(loop_rate)
        # Subscribe to biotac data:
        rospy.Subscriber('/biotac_pub',BioTacHand,self.biotac_cb)
        # Create publisher to publish contact position and force
        contact_pub_arr=[]
        fingers=['index_tip','middle_tip','ring_tip','thumb_tip']
        for i in range(4):
            contact_pub_arr.append(rospy.Publisher('biotac/'+str(fingers[i])+'/contact_data',
                                                   WrenchStamped,queue_size=1))

        self.fingers=fingers
        self.contact_pub_arr=contact_pub_arr
        
        self.got_biotac_data=False
        # initializing sensor parameters:
        self.num_electrodes=19
        self.got_initial_offsets=False
        self.elect_positions=np.zeros((19,3))
        self.update_elect_positions()
        self.pres_thresh_gain=1.1
        
        self.IN_CONTACT=[False for i in range(4)]# ll4ma lab has 4 sensors
        self.electrode_data_norm=[np.zeros(19) for i in range(4)]
        self.tare_pdc=[0.0 for i in range(4)]
        self.rad=5.5

        
    def update_elect_positions(self):
        elect_position=np.zeros((20,4))
        elect_position[1][1] = 0.993;
	elect_position[1][2] = -4.855;
	elect_position[1][3] = -1.116;
	elect_position[2][1] = -2.700;
	elect_position[2][2] = -3.513;
	elect_position[2][3] = -3.670;
	elect_position[3][1] = -6.200;
	elect_position[3][2] = -3.513;
	elect_position[3][3] = -3.670;
	elect_position[4][1] = -8.000;
	elect_position[4][2] = -4.956;
	elect_position[4][3] = -1.116;
	elect_position[5][1] = -10.500;
	elect_position[5][2] = -3.513;
	elect_position[5][3] = -3.670;
	elect_position[6][1] = -13.400;
	elect_position[6][2] = -4.956;
	elect_position[6][3] = -1.116;
	elect_position[7][1] = 4.763;
	elect_position[7][2] = 0.000;
	elect_position[7][3] = -2.330;
	elect_position[8][1] = 3.031;
	elect_position[8][2] = -1.950;
	elect_position[8][3] = -3.330;
	elect_position[9][1] = 3.031;
	elect_position[9][2] = 1.950;
	elect_position[9][3] = -3.330;
	elect_position[10][1] = 1.299;
	elect_position[10][2] = 0.000;
	elect_position[10][3] = -4.330;
	elect_position[11][1] = 0.993;
	elect_position[11][2] = 4.855;
	elect_position[11][3] = -1.116;
	elect_position[12][1] = -2.700;
	elect_position[12][2] = 3.513;
	elect_position[12][3] = -3.670;
	elect_position[13][1] = -6.200;
	elect_position[13][2] = 3.513;
	elect_position[13][3] = -3.670;
	elect_position[14][1] = -8.000;
	elect_position[14][2] = 4.956;
	elect_position[14][3] = -1.116;
	elect_position[15][1] = -10.500;
	elect_position[15][2] = 3.513;
	elect_position[15][3] = -3.670;
	elect_position[16][1] = -13.400;
	elect_position[16][2] = 4.956;
	elect_position[16][3] = -1.116;
	elect_position[17][1] = -2.800;
	elect_position[17][2] = 0.000;
	elect_position[17][3] = -5.080;
	elect_position[18][1] = -9.800;
	elect_position[18][2] = 0.000;
	elect_position[18][3] = -5.080;
	elect_position[19][1] = -13.600;
	elect_position[19][2] = 0.000;
	elect_position[19][3] = -5.080;
        self.elect_positions=elect_position[1:20,1:4]

        # Normals:
        elect_normal=np.zeros((20,4))
        elect_normal[1][1] = 0.196;
	elect_normal[1][2] = -0.956;
	elect_normal[1][3] = 0.220;
	elect_normal[2][1] = 0.0;
	elect_normal[2][2] = -0.692;
	elect_normal[2][3] = -0.722;
	elect_normal[3][1] = 0.0;
	elect_normal[3][2] = -0.692;
	elect_normal[3][3] = -0.722;
	elect_normal[4][1] = 0.0;
	elect_normal[4][2] = -0.976;
	elect_normal[4][3] = -0.220;
	elect_normal[5][1] = 0.0;
	elect_normal[5][2] = -0.976;
	elect_normal[5][3] = -0.220;
	elect_normal[6][1] = 0.5;
	elect_normal[6][2] = 0.0;
	elect_normal[6][3] = -0.866;
	elect_normal[7][1] = 0.5;
	elect_normal[7][2] = 0.0;
	elect_normal[7][3] = -0.866;
	elect_normal[8][1] = 0.5;
	elect_normal[8][2] = 0.0;
	elect_normal[8][3] = -0.866;
	elect_normal[9][1] = 0.5;
	elect_normal[9][2] = 0.0;
	elect_normal[9][3] = -0.866;
	elect_normal[10][1] = 0.5;
	elect_normal[10][2] = 0.0;
	elect_normal[10][3] = -0.866;
	elect_normal[11][1] = 0.196;
	elect_normal[11][2] = 0.956;
	elect_normal[11][3] = -0.220;
	elect_normal[12][1] = 0.0;
	elect_normal[12][2] = 0.692;
	elect_normal[12][3] = -0.722;
	elect_normal[13][1] = 0.0;
	elect_normal[13][2] = 0.692;
	elect_normal[13][3] = -0.722;
	elect_normal[14][1] = 0.0;
	elect_normal[14][2] = 0.976;
	elect_normal[14][3] = -0.220;
	elect_normal[15][1] = 0.0;
	elect_normal[15][2] = 0.692;
	elect_normal[15][3] = -0.722;
	elect_normal[16][1] = 0.0;
	elect_normal[16][2] = 0.976;
	elect_normal[16][3] = -0.220;
	elect_normal[17][1] = 0.0;
	elect_normal[17][2] = 0.0;
	elect_normal[17][3] = -1.000;
	elect_normal[18][1] = 0.0;
	elect_normal[18][2] = 0.0;
	elect_normal[18][3] = -1.000;
	elect_normal[19][1] = 0.0;
	elect_normal[19][2] = 0.0;
	elect_normal[19][3] = -1.000;
        self.elect_normal=elect_normal[1:20,1:4]
        
    def biotac_cb(self,msg):
        if(not self.got_biotac_data):
            self.num_sensors=len(msg.bt_data)
            self.bio_data=[[] for i in range(self.num_sensors)]
            self.tare=[[] for i in range(self.num_sensors)]
            self.got_biotac_data=True
        for i in range(self.num_sensors):
            if(self.got_initial_offsets):
                if( msg.bt_data[i].pdc_data>=self.tare_pdc[i]):
                    self.IN_CONTACT[i]=True
                else:
                    self.IN_CONTACT[i]=False
            self.bio_data[i]=msg.bt_data[i]

    def get_initial_offsets(self,idx):
        self.tare[idx]=np.array(self.bio_data[idx].electrode_data)
        self.tare_pdc[idx]=copy.deepcopy(self.pres_thresh_gain*self.bio_data[idx].pdc_data)
        #print 'Stored initial offsets for electrodes'
        self.got_initial_offsets=True

    def compute_contact_position(self,idx,R):
        # Computes the contact position of idx sensor:
        #weights=np.zeros(self.num_electrodes)
        electrode_data=self.electrode_data_norm[idx]**2 # squared as per paper
        normalized_data=electrode_data/np.sum(electrode_data)
        contact_pt=np.matrix(normalized_data)*self.elect_positions #[1x19]* [19*3]= [1x3] centroid
        contact_pt=np.ravel(contact_pt)
        v_norm=0.0
        r=R
        if(contact_pt[0]>0.0):
            for j in range(3):
                v_norm+=contact_pt[j]**2
            contact_pt=contact_pt*(r/np.sqrt(v_norm))
        else:
            j=1
            while(j<3):
                v_norm+=contact_pt[j]**2
                j+=1
            j=1
            while(j<3):
                contact_pt[j]=contact_pt[j]*(r/np.sqrt(v_norm))
                j+=1
                
        
        return np.ravel(contact_pt)#/1000.0

    def compute_contact_normal(self,idx):
        electrode_data=self.electrode_data_norm[idx]**2
        normal=np.matrix(electrode_data)*self.elect_normal
        normal_vec=np.ravel(normal)/np.linalg.norm(normal)
    
        return np.ravel(normal_vec)#/1000.0
    
    def compute_norm_data(self,idx):
        self.electrode_data_norm[idx]=np.array(self.bio_data[idx].electrode_data-self.tare[idx],
                                                     dtype=np.float32)
        
    def pub_wrench(self,position,normal,idx):
        new_pose=WrenchStamped()
    
        new_pose.header.frame_id=self.fingers[idx]
        new_pose.header.stamp=rospy.Time.now()
        new_pose.wrench.force.x=position[0]
        new_pose.wrench.force.y=position[1]
        new_pose.wrench.force.z=position[2]
        new_pose.wrench.torque.x=normal[0]
        new_pose.wrench.torque.y=normal[1]
        new_pose.wrench.torque.z=normal[2]
        self.contact_pub_arr[idx].publish(new_pose)

    def get_contact_data(self):
        if self.got_biotac_data:
            if not self.got_initial_offsets:
                for idx in range(self.num_sensors):
                    self.get_initial_offsets(idx)
            else:
                for idx in range(self.num_sensors):
                    self.compute_norm_data(idx)
                    if(self.IN_CONTACT[idx]):
                        position=self.compute_contact_position(idx,self.rad)
                        normal=self.compute_contact_normal(idx)+position
                    else:
                        position=np.zeros(3)
                        normal=np.zeros(3)
                    self.pub_wrench(position,normal,idx)
        #return position,normal
    '''
    def pub_contact_data_all_sensors(self):
        for idx in range(self.num_sensors):
            p,n=self.get_contact_data(idx)
            self.pub_wrench(p,n)
    '''
def main():
    biotac=biotacSensing()
    while not rospy.is_shutdown():
        biotac.get_contact_data()
        biotac.rate.sleep()

main()

